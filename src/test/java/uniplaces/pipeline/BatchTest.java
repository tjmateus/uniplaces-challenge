package uniplaces.pipeline;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import uniplaces.pipeline.managers.layers.batch.BatchManagerFactory;
import uniplaces.pipeline.managers.layers.batch.BatchManager;
import uniplaces.pipeline.managers.exceptions.ManagerException;


/**
 * Created by tmateus.
 */
public class BatchTest
{

    public static void main(String[] args) throws ManagerException
    {
        BatchManager batchManager = BatchManagerFactory.build();

        batchManager.createView();

        Dataset<Row> dataFrame = batchManager.getView();

        dataFrame.show();
    }

}
