package uniplaces.pipeline;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by tmateus.
 */
public class SocketServer
{

    static Set<Socket> sockets = new HashSet<>();

    public static void main(String args[]) throws Exception {
        ServerSocket serverSocket = null;
        Socket socket = null;

        try {
            serverSocket = new ServerSocket(6789);
        } catch (IOException e) {
            e.printStackTrace();

        }
        while (true) {
            try {
                socket = serverSocket.accept();
                sockets.add(socket);
            } catch (IOException e) {
                System.out.println("I/O error: " + e);
            }
            // new threa for a client
            new EchoThread(socket).start();
        }
    }

    public static class EchoThread extends Thread {
        protected Socket socket;

        public EchoThread(Socket clientSocket) {
            this.socket = clientSocket;
        }

        public void run() {
            InputStream inp = null;
            BufferedReader brinp = null;
            DataOutputStream out = null;
            try {
                inp = socket.getInputStream();
                brinp = new BufferedReader(new InputStreamReader(inp));

            } catch (IOException e) {
                return;
            }
            String line;
            while (true) {
                try {
                    line = brinp.readLine();
                    if ((line == null) || line.equalsIgnoreCase("QUIT")) {
                        socket.close();
                        return;
                    } else {
                        for(Socket socket : sockets)
                        {
                            if(!this.socket.equals(socket) && !socket.isClosed())
                            {
                                out = new DataOutputStream(socket.getOutputStream());
                                if (!line.isEmpty())
                                {
                                    out.writeBytes(line.trim() + "\r");
                                    out.flush();
                                }
                            }
                        }

                        System.out.println("Received: " + line + ">");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }
    }

}
