package uniplaces.pipeline;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import uniplaces.pipeline.managers.exceptions.ManagerException;
import uniplaces.pipeline.managers.layers.serving.ServingManager;
import uniplaces.pipeline.managers.layers.serving.ServingManagerFactory;
import uniplaces.pipeline.model.Event;
import uniplaces.pipeline.model.Guest;
import uniplaces.pipeline.model.View;

import java.util.List;


/**
 * Created by tmateus.
 */
public class ServingTest
{

    public static void main(String[] args) throws ManagerException
    {
        ServingManager servingManager = ServingManagerFactory.build();

        List<Guest> guests = servingManager.getView(View.MERGE_VIEW);

        Gson gson = new GsonBuilder()
                .setDateFormat("dd/MM/yyyy").create();
        for(Guest guest : guests)
        {
            System.out.println(gson.toJson(guest));
        }
    }

}
