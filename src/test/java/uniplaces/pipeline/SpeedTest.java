package uniplaces.pipeline;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import uniplaces.pipeline.datasource.DataSource;
import uniplaces.pipeline.datasource.DataSourceFactory;
import uniplaces.pipeline.datasource.exceptions.DataSourceException;
import uniplaces.pipeline.managers.exceptions.ManagerException;
import uniplaces.pipeline.managers.layers.speed.SpeedManager;
import uniplaces.pipeline.managers.layers.speed.SpeedManagerFactory;


/**
 * Created by tmateus.
 */
public class SpeedTest
{

    public static void main(String[] args) throws ManagerException
    {
        //initStreaming();

        SpeedManager speedManager = SpeedManagerFactory.build();

        //speedManager.createView();

        Dataset<Row> dataFrame = speedManager.getView();

        dataFrame.show();
    }



    private static void initStreaming() throws ManagerException
    {
        SpeedManager speedManager = SpeedManagerFactory.build();

        speedManager.initStreamingListener();
    }

}
