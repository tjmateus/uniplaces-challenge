package uniplaces.pipeline;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import uniplaces.pipeline.datasource.exceptions.DataSourceException;
import uniplaces.pipeline.managers.layers.speed.base.SpeedManagerUtils;
import uniplaces.pipeline.model.Location;
import uniplaces.pipeline.services.storage.StorageService;
import uniplaces.pipeline.services.storage.StorageServiceFactory;
import uniplaces.pipeline.services.storage.exceptions.StorageServiceException;

import java.io.FileNotFoundException;
import java.io.FileReader;


/**
 * Created by tmateus.
 */
public class DataSourceTest
{

    private static String REGISTER_FILE_PATH = "/home/supervisor/Desktop/Uniplaces/data-engineer-test-master/data/registered-events.csv";
    private static String UPDATE_FILE_PATH = "/home/supervisor/Desktop/Uniplaces/data-engineer-test-master/data/updated_events.csv";

    public static void main(String[] args) throws StorageServiceException, DataSourceException
    {
        insertEvents(REGISTER_FILE_PATH);
        //insertEvents(UPDATE_FILE_PATH);
    }

    private static boolean insertEvents(String localFile) throws StorageServiceException
    {
        StorageService storageService = StorageServiceFactory.build();

        try
        {
            JsonParser parser = new JsonParser();
            JsonArray jsonArray = (JsonArray) parser.parse(new FileReader(localFile));

            int i = 0;
            for(JsonElement jsonElement : jsonArray)
            {
                Location nameLocation = new Location(Location.Entity.ENTITY_EMAIL, Location.Fact.FACT_EVENT);
                String newJson = SpeedManagerUtils.buildAtomicEvents(jsonElement.toString(), nameLocation);

                System.out.println("Adding " + newJson);

                storageService.insertEvent(newJson, nameLocation);

                i++;
                if(i == 2)
                    break;
            }

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (DataSourceException e)
        {
            e.printStackTrace();
        }

        return true;

    }

}
