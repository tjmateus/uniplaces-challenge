package individual;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.InputStream;


/**
 * Created by tmateus.
 */
public class ApacheAvroTest
{

    public static void main(String argv[]) throws Exception {


        //String json = "{\"timestamp\":\"19/03/2016\",\"action\":\"guest_registered\",\"guest_id\":\"new_id\",\"name\":\"Mariana\",\"email\":\"lobortis.ultrices.Vivamus@Suspendisse.com\"}";

        String json = "{\"timestamp\":\"15/04/2030\",\"action\":\"guest_name_updated\",\"guest_id\":\"E44C1E7F-4619-E251-8677-35E1C3EF3131\",\"name\":\"Tiago\"}";

        InputStream input = new ByteArrayInputStream(json.getBytes());
        DataInputStream din = new DataInputStream(input);

        Schema schema = new Schema.Parser().parse(new File("/home/supervisor/git/uniplaces-challenge/src/main/resources/schema/schema-event.avsc"));

        Decoder decoder = DecoderFactory.get().jsonDecoder(schema, din);

        GenericRecord event = new GenericData.Record(schema);

        DatumReader<GenericRecord> reader = new GenericDatumReader<>(schema);

        try
        {
            GenericRecord datum = reader.read(event, decoder);
            System.out.println(datum);
            System.out.println(datum.get("action"));

            Schema schemaWrite = new Schema.Parser().parse(new File("/home/supervisor/git/uniplaces-challenge/src/main/resources/schema/schema-event-write.avsc"));

            GenericRecord eventWrite = new GenericData.Record(schemaWrite);

            System.out.println(eventWrite);

        }
        catch(AvroRuntimeException e)
        {
            System.out.println(e.getMessage());
        }



    }

}
