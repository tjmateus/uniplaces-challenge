package individual;

import com.mongodb.*;

import java.net.UnknownHostException;
import java.util.Date;


/**
 * Created by tmateus.
 */
public class MongoDBTest
{

    private final static String HOST = "localhost";
    private final static Integer PORT = 27017;
    private final static String NAME = "test";


    public static void main(String[] args) throws UnknownHostException
    {
        MongoClient mongo = new MongoClient( HOST , PORT );

        DB db = mongo.getDB(NAME);

        DBCollection table = db.getCollection("test");

        createView(table);

        System.out.println("Created View.");

        getView(table);

        updateView(table);

        System.out.println("Updated View.");

        getView(table);

    }

    private static void getView(DBCollection table)
    {
        /**** Find and display ****/
        BasicDBObject searchQuery = new BasicDBObject().append("name", "mkyong-updated");

        DBCursor cursor = table.find(searchQuery);

        while (cursor.hasNext())
        {
            System.out.println(cursor.next());
        }
    }

    private static boolean updateView(DBCollection table)
    {
        /**** Update ****/
        // search document where name="mkyong" and update it with new values
        BasicDBObject query = new BasicDBObject();
        query.put("name", "mkyong");

        BasicDBObject newDocument = new BasicDBObject();
        newDocument.put("name", "mkyong-updated");

        BasicDBObject updateObj = new BasicDBObject();
        updateObj.put("$set", newDocument);

        table.update(query, updateObj);

        return true;
    }

    private static boolean createView(DBCollection table)
    {
        /**** Insert ****/
        // create a document to store key and value
        BasicDBObject document = new BasicDBObject();
        document.put("name", "mkyong");
        document.put("createdDate", new Date());
        table.insert(document);

        return true;
    }

}
