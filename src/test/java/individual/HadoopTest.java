package individual;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import uniplaces.pipeline.managers.layers.batch.exceptions.BatchManagerException;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by tmateus.
 */
public class HadoopTest
{

    private static final String CONNECTION_URL = "hdfs://localhost:54310/";

    public static void main(String[] args) throws IOException, URISyntaxException, BatchManagerException
    {
        /*String localFile = "/home/supervisor/Desktop/data-engineer-test-master/data/registered-events.csv";
        String directory = "/uniplaces/test";

        //List files
        System.out.println("BEFORE =================");
        listFiles(directory);

        putFiles(localFile, directory);

        System.out.println("AFTER ==================");
        listFiles(directory);*/

        for(String path : getFilePaths())
        {
            System.out.println(path);
        }

    }

    private static Set<String> getFilePaths() throws BatchManagerException
    {
        try
        {
            Set<String> filePaths = new HashSet<>();

            org.apache.hadoop.conf.Configuration hadoopConfiguration = new org.apache.hadoop.conf.Configuration();
            hadoopConfiguration.set("fs.defaultFS", CONNECTION_URL);

            FileSystem fileSystem = FileSystem.get(hadoopConfiguration);

            Path filePath = new Path(CONNECTION_URL + "uniplaces");

            //3. Get the metadata of the desired directory
            FileStatus[] fileStatus = fileSystem.listStatus(filePath);

            getFilePaths(fileSystem, fileStatus, filePaths);

            return filePaths;
        }
        catch (IOException e)
        {
            throw new BatchManagerException(e.getMessage(), e);
        }
    }

    private static FileStatus getFilePaths(FileSystem fileSystem, FileStatus[] fileStatusList, Set<String> filePaths) throws IOException
    {
        if(fileStatusList != null)
        {
            Path[] paths = FileUtil.stat2Paths(fileStatusList);

            for(Path path : paths)
            {
                if(path.toString().endsWith(".json"))
                {
                    filePaths.add(path.toString());
                }
                else
                {
                    getFilePaths(fileSystem, fileSystem.listStatus(path), filePaths);
                }
            }
        }

        return null;
    }

    private static void listFiles(String directory) throws URISyntaxException, IOException
    {
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(new URI(CONNECTION_URL + directory), conf);
        FileStatus[] fileStatus = fs.listStatus(new Path(CONNECTION_URL + directory));

        for(FileStatus status : fileStatus)
        {
            System.out.println(status.getPath().toString());
        }
    }

    private static boolean putFiles(String localFile, String destination) throws IOException
    {
        //Get configuration of Hadoop system
        Configuration conf = new Configuration();
        conf.set("fs.defaultFS", CONNECTION_URL);

        System.out.println("Connecting to -- "+conf.get("fs.defaultFS"));

        FileSystem fs = FileSystem.get(conf);

        Path outFile = new Path(CONNECTION_URL + destination + "/test");

        //Input stream for the file in local file system to be written to HDFS
        InputStream in = new BufferedInputStream(new FileInputStream(localFile));

        // Check if input/output are valid
        if (fs.exists(outFile))
        {
            System.out.println("Appending file...");

            // Read from and write to new file
            byte buffer[] = new byte[256];
            try (FSDataOutputStream out = fs.append(outFile))
            {
                int bytesRead = 0;
                while ((bytesRead = in.read(buffer)) > 0)
                {
                    out.write(buffer, 0, bytesRead);
                }
            }
            catch (IOException e)
            {
                System.out.println("Error while appending file");
                e.printStackTrace();
            }
        }
        else
        {
            System.out.println("Creating file...");

            // Read from and write to new file
            byte buffer[] = new byte[256];
            try (FSDataOutputStream out = fs.create(outFile))
            {
                int bytesRead = 0;
                while ((bytesRead = in.read(buffer)) > 0)
                {
                    out.write(buffer, 0, bytesRead);
                }
            }
            catch (IOException e)
            {
                System.out.println("Error while copying file");
            }
        }

        return true;
    }

    static void printAndExit(String str) {
        System.err.println( str );
        System.exit(1);
    }

}
