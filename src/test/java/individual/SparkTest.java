package individual;

import com.mongodb.spark.MongoSpark;
import com.mongodb.spark.config.ReadConfig;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.regex.Pattern;


/**
 * Created by tmateus.
 */
public class SparkTest
{
    private static final Pattern SPACE = Pattern.compile(" ");

    private static SparkConf conf;
    private static SparkSession spark;
    private static JavaSparkContext sc;

    public static void main(String[] args) throws Exception {

        //Source file in the local file system
        //String localSrc = "/home/supervisor/Desktop/data-engineer-test-master/data/registered-events.csv";
        String localSrc = "hdfs://localhost:54310/uniplaces/registers/registered-events.csv";

        conf = new SparkConf().setAppName("Simple Application").setMaster("local")
                .set("spark.mongodb.input.uri", "mongodb://127.0.0.1/test.test")
                .set("spark.mongodb.output.uri", "mongodb://127.0.0.1/test.test");

        sc = new JavaSparkContext(conf);

        spark = SparkSession
                .builder()
                .appName("Java Spark SQL data sources example")
                .config(conf)
                .getOrCreate();

        runJsonDatasetExample(spark);

        //loadViewFromDatabase(spark);

        /*JavaSparkContext sc = new JavaSparkContext(conf);
        JavaRDD<String> logData = sc.textFile(localSrc).cache();

        countLetters(logData, "a");

        sc.stop();

        System.out.println();*/

    }

    private static void runJsonDatasetExample(SparkSession spark) {
        // $example on:json_dataset$
        // A JSON dataset is pointed to by path.
        // The path can be either a single text file or a directory storing text files
        //Dataset<Row> people = spark.read().json("/home/supervisor/Desktop/test.json");

        //JavaRDD<String> textFile = sc.textFile("hdfs://localhost:54310/uniplaces/test/test1.json");
        Dataset<Row> people = spark.read().json("hdfs://localhost:54310/uniplaces/test/test.json");

        // The inferred schema can be visualized using the printSchema() method
        people.printSchema();
        // root
        //  |-- age: long (nullable = true)
        //  |-- name: string (nullable = true)

        // Creates a temporary view using the DataFrame
        people.createOrReplaceTempView("guest");

        // SQL statements can be run by using the sql methods provided by spark
        Dataset<Row> namesDF = spark.sql("SELECT name FROM guest");
        namesDF.show();

        saveViewToDatabase(namesDF);

    }

    private static boolean saveViewToDatabase(Dataset<Row> namesDF)
    {
        MongoSpark.write(namesDF).option("collection", "test").mode("overwrite").save();

        return true;
    }

    private static boolean loadViewFromDatabase(SparkSession sparkSession)
    {
        JavaSparkContext jsc = new JavaSparkContext(sparkSession.sparkContext());

        // Loading data with a custom ReadConfig
        /*Map<String, String> readOverrides = new HashMap<String, String>();
        readOverrides.put("collection", "spark");
        readOverrides.put("readPreference.name", "secondaryPreferred");
        ReadConfig readConfig = ReadConfig.create(jsc).withOptions(readOverrides);*/

        Dataset<Row> implicitDS = MongoSpark.load(jsc).toDF();

        implicitDS.show();

        return true;
    }

    private static void countLetters(JavaRDD<String> logData, String letter)
    {
        final String finalLetter = letter;

        long num = logData.filter(new Function<String, Boolean>() {
            public Boolean call(String s) { return s.contains(finalLetter); }
        }).count();

        System.out.println("Lines with "+letter+": " + num);
    }

}
