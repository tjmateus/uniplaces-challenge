package uniplaces.pipeline.rest.model;

import uniplaces.pipeline.model.Guest;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by tmateus.
 */
public class GuestsFacade
{
    private List<GuestFacade> guests;

    public GuestsFacade(List<Guest> guests)
    {
        List<GuestFacade> guestFacades = new ArrayList<>();

        for(Guest guest : guests)
        {
            guestFacades.add(new GuestFacade(guest));
        }

        this.guests = guestFacades;
    }

    public List<GuestFacade> getGuests()
    {
        return guests;
    }

    public void setGuests(List<GuestFacade> guests)
    {
        this.guests = guests;
    }
}
