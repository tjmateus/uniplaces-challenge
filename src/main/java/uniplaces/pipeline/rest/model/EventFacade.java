package uniplaces.pipeline.rest.model;

import com.google.gson.annotations.SerializedName;
import uniplaces.pipeline.model.Event;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by tmateus.
 */
public class EventFacade implements Serializable
{

    private String timestamp;
    private String action;

    @SerializedName("guest_id")
    private String guestId;
    private String name;
    private String email;

    public EventFacade()
    {

    }

    public EventFacade(Event event)
    {
        this.timestamp = event.getTimestamp();
        this.action = event.getAction();
        this.guestId =event.getGuestId();
        this.name = event.getName();
        this.email = event.getEmail();
    }

    public Event toEvent()
    {
        Event event = new Event();

        event.setTimestamp(timestamp);
        event.setAction(action);
        event.setGuestId(guestId);
        event.setName(name);
        event.setEmail(email);

        return event;
    }

    public static List<Event> toEvents(List<EventFacade> eventsFacade)
    {
        List<Event> events = new ArrayList<>();

        for(EventFacade eventFacade : eventsFacade)
        {
            events.add(eventFacade.toEvent());
        }

        return events;
    }

    public String getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(String timestamp)
    {
        this.timestamp = timestamp;
    }

    public String getAction()
    {
        return action;
    }

    public void setAction(String action)
    {
        this.action = action;
    }

    public String getGuestId()
    {
        return guestId;
    }

    public void setGuestId(String guestId)
    {
        this.guestId = guestId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }
}
