package uniplaces.pipeline.rest.model;

import com.google.gson.annotations.SerializedName;
import uniplaces.pipeline.model.Guest;

import java.util.Date;


/**
 * Created by tmateus.
 */
public class GuestFacade
{

    private String id;
    private String name;
    private String email;
    @SerializedName("created_at")
    private Date createdAt;
    @SerializedName("updated_at")
    private Date updatedAt;

    public GuestFacade(Guest guest)
    {
        this.id = guest.getId();
        this.name = guest.getName();
        this.email = guest.getEmail();
        this.createdAt = guest.getCreatedAt();
        this.updatedAt = guest.getUpdatedAt();
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public Date getCreatedAt()
    {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt)
    {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt()
    {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt)
    {
        this.updatedAt = updatedAt;
    }
}
