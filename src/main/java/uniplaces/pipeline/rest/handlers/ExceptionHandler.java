package uniplaces.pipeline.rest.handlers;

import com.google.gson.Gson;
import uniplaces.pipeline.rest.exceptions.RestServiceException;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by tmateus.
 */
public class ExceptionHandler
{

    //Create JSON with exception message and code.
    public static String handle(RestServiceException exception)
    {
        exception.printStackTrace();

        Gson gson = new Gson();

        Map<String,String> map = new HashMap<>();

        map.put("message", exception.getMessage());

        return gson.toJson(map);
    }

}
