package uniplaces.pipeline.rest.exceptions;

import uniplaces.pipeline.exceptions.PipelineException;


/**
 * @author bantunes
 */
public class RestServiceException extends PipelineException
{

    private static final long serialVersionUID = -828821163543156842L;

    public RestServiceException()
    {
        super();
    }

    public RestServiceException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public RestServiceException(String message)
    {
        super(message);
    }

    public RestServiceException(Throwable cause)
    {
        super(cause);
    }
    
}
