package uniplaces.pipeline.rest.services;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import uniplaces.pipeline.rest.exceptions.RestServiceException;
import uniplaces.pipeline.rest.handlers.ExceptionHandler;
import uniplaces.pipeline.rest.model.EventFacade;
import uniplaces.pipeline.rest.requests.json.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Type;
import java.util.List;


/**
 * Created by tmateus.
 */
@Path("/pipeline")
@Api(value = "/pipeline", description = "Uniplaces Pipeline")
public class RestService
{

    @GET
    @Path("/layer/serving/view/speed")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get serving speed view.")
    public Response getServingSpeedView()
    {
        GetSpeedViewJsonRequest request = new GetSpeedViewJsonRequest();

        try
        {
            return Response.status(Response.Status.OK).entity(request.execute()).build();
        }
        catch (RestServiceException e)
        {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ExceptionHandler.handle(e)).build();
        }
    }

    @GET
    @Path("/layer/serving/view/batch")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get serving batch view.")
    public Response getServingBatchView()
    {
        GetBatchViewJsonRequest request = new GetBatchViewJsonRequest();

        try
        {
            return Response.status(Response.Status.OK).entity(request.execute()).build();
        }
        catch (RestServiceException e)
        {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ExceptionHandler.handle(e)).build();
        }
    }

    @GET
    @Path("/layer/serving/view")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get serving view.")
    public Response getServingMergeView()
    {
        GetMergeViewJsonRequest request = new GetMergeViewJsonRequest();

        try
        {
            return Response.status(Response.Status.OK).entity(request.execute()).build();
        }
        catch (RestServiceException e)
        {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ExceptionHandler.handle(e)).build();
        }
    }
    
    @POST
    @Path("/layer/batch")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Process batch job.")
    public Response processBatchJob()
    {
        ProcessBatchJobJsonRequest request = new ProcessBatchJobJsonRequest();

        try
        {
            return Response.status(Response.Status.OK).entity(request.execute()).build();
        }
        catch (RestServiceException e)
        {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ExceptionHandler.handle(e)).build();
        }
    }

    @POST
    @Path("/datasource/event/insert")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Insert event.")
    public Response insertEvent(@ApiParam(value = "Event Representation", required = true) String eventFacadeJson)
    {
        EventFacade eventFacade = new Gson().fromJson(eventFacadeJson, EventFacade.class);
        InsertEventJsonRequest request = new InsertEventJsonRequest(eventFacade);

        try
        {
            return Response.status(Response.Status.OK).entity(request.execute()).build();
        }
        catch (RestServiceException e)
        {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ExceptionHandler.handle(e)).build();
        }
    }

    @POST
    @Path("/datasource/events/insert")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Insert events.")
    public Response insertEvents(@ApiParam(value = "Events Representation", required = true) String eventsFacadeJson)
    {
        Type eventsFacadeListType = new TypeToken<List<EventFacade>>() {}.getType();
        List<EventFacade> eventsFacade = new Gson().fromJson(eventsFacadeJson, eventsFacadeListType);

        InsertEventsJsonRequest request = new InsertEventsJsonRequest(eventsFacade);

        try
        {
            return Response.status(Response.Status.OK).entity(request.execute()).build();
        }
        catch (RestServiceException e)
        {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ExceptionHandler.handle(e)).build();
        }
    }

    @POST
    @Path("/layer/speed/listener/start")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Start event listener.")
    public Response startEventListener()
    {
        StartEventListenerJsonRequest request = new StartEventListenerJsonRequest();

        try
        {
            return Response.status(Response.Status.OK).entity(request.execute()).build();
        }
        catch (RestServiceException e)
        {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ExceptionHandler.handle(e)).build();
        }
    }

}
