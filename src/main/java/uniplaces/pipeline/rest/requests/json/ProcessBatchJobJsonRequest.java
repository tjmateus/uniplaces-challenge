package uniplaces.pipeline.rest.requests.json;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import uniplaces.pipeline.managers.exceptions.ManagerException;
import uniplaces.pipeline.managers.layers.batch.BatchManager;
import uniplaces.pipeline.managers.layers.batch.BatchManagerFactory;
import uniplaces.pipeline.rest.exceptions.RestServiceException;
import uniplaces.pipeline.rest.requests.Request;


/**
 * Created by tmateus.
 */
public class ProcessBatchJobJsonRequest extends Request
{

    public String execute() throws RestServiceException
    {
        try
        {
            BatchManager batchManager = BatchManagerFactory.build();

            JsonObject json = new JsonObject();

            if(batchManager.createView())
            {
                json.add("status", new JsonPrimitive("SUCCESS"));
            }
            else
            {
                json.add("status", new JsonPrimitive("FAILURE"));
            }

            return json.toString();
        }
        catch (ManagerException e)
        {
            throw new RestServiceException(e.getMessage(), e);
        }
    }

}
