package uniplaces.pipeline.rest.requests.json;

import com.google.gson.GsonBuilder;
import uniplaces.pipeline.managers.exceptions.ManagerException;
import uniplaces.pipeline.managers.layers.serving.ServingManager;
import uniplaces.pipeline.managers.layers.serving.ServingManagerFactory;
import uniplaces.pipeline.model.View;
import uniplaces.pipeline.rest.exceptions.RestServiceException;
import uniplaces.pipeline.rest.model.GuestsFacade;
import uniplaces.pipeline.rest.requests.Request;


/**
 * Created by tmateus.
 */
public class GetMergeViewJsonRequest extends Request
{

    public String execute() throws RestServiceException
    {
        try
        {
            ServingManager servingManager = ServingManagerFactory.build();

            return new GsonBuilder().setDateFormat("dd/MM/yyyy").create().toJson(new GuestsFacade(servingManager.getView(View.MERGE_VIEW)));
        }
        catch (ManagerException e)
        {
            throw new RestServiceException(e.getMessage(), e);
        }
    }

}
