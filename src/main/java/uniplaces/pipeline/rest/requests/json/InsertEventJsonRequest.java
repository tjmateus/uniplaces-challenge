package uniplaces.pipeline.rest.requests.json;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import uniplaces.pipeline.datasource.DataSourceFactory;
import uniplaces.pipeline.datasource.exceptions.DataSourceException;
import uniplaces.pipeline.rest.exceptions.RestServiceException;
import uniplaces.pipeline.rest.model.EventFacade;
import uniplaces.pipeline.rest.requests.Request;


/**
 * Created by tmateus.
 */
public class InsertEventJsonRequest extends Request
{
    private EventFacade eventFacade;

    public InsertEventJsonRequest(EventFacade eventFacade)
    {
        this.eventFacade = eventFacade;
    }

    public String execute() throws RestServiceException
    {
        try
        {
            JsonObject json = new JsonObject();

            if(DataSourceFactory.build().insertEvent(eventFacade.toEvent()))
            {
                json.add("status", new JsonPrimitive("SUCCESS"));
            }
            else
            {
                json.add("status", new JsonPrimitive("FAILURE"));
            }

            return json.toString();
        }
        catch (DataSourceException e)
        {
            throw new RestServiceException(e.getMessage(), e);
        }
    }

}
