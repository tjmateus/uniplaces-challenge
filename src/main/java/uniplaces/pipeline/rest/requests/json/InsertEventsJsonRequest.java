package uniplaces.pipeline.rest.requests.json;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import uniplaces.pipeline.datasource.DataSourceFactory;
import uniplaces.pipeline.datasource.exceptions.DataSourceException;
import uniplaces.pipeline.rest.exceptions.RestServiceException;
import uniplaces.pipeline.rest.model.EventFacade;
import uniplaces.pipeline.rest.requests.Request;

import java.util.List;


/**
 * Created by tmateus.
 */
public class InsertEventsJsonRequest extends Request
{
    private List<EventFacade> eventsFacade;

    public InsertEventsJsonRequest(List<EventFacade> eventsFacade)
    {
        this.eventsFacade = eventsFacade;
    }

    public String execute() throws RestServiceException
    {
        try
        {
            JsonObject json = new JsonObject();

            if(DataSourceFactory.build().insertEvents(EventFacade.toEvents(eventsFacade)))
            {
                json.add("status", new JsonPrimitive("SUCCESS"));
            }
            else
            {
                json.add("status", new JsonPrimitive("FAILURE"));
            }

            return json.toString();
        }
        catch (DataSourceException e)
        {
            throw new RestServiceException(e.getMessage(), e);
        }
    }

}
