package uniplaces.pipeline.rest.requests.json;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.slf4j.LoggerFactory;
import uniplaces.pipeline.managers.exceptions.ManagerException;
import uniplaces.pipeline.managers.layers.speed.SpeedManagerFactory;
import uniplaces.pipeline.rest.exceptions.RestServiceException;
import uniplaces.pipeline.rest.requests.Request;


/**
 * Created by tmateus.
 */
public class StartEventListenerJsonRequest extends Request
{

    public String execute() throws RestServiceException
    {
        new EventListenerWorker().start();

        JsonObject json = new JsonObject();

        json.add("status", new JsonPrimitive("SUCCESS"));

        return json.toString();
    }

    class EventListenerWorker extends Thread
    {

        public void run()
        {
            try
            {
                SpeedManagerFactory.build().initStreamingListener();
            }
            catch (ManagerException e)
            {
                LoggerFactory.getLogger(StartEventListenerJsonRequest.class).error("Couldn't start event listener.");
            }
        }
    }

}
