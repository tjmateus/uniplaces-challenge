package uniplaces.pipeline.managers;

import org.slf4j.LoggerFactory;
import uniplaces.pipeline.services.exceptions.ServiceException;
import uniplaces.pipeline.services.spark.SparkService;
import uniplaces.pipeline.services.spark.SparkServiceFactory;
import uniplaces.pipeline.services.storage.StorageService;
import uniplaces.pipeline.services.storage.StorageServiceFactory;


/**
 * Created by tmateus.
 */
public abstract class Manager
{

    protected static StorageService storageService;

    protected static SparkService sparkService;

    static
    {
        try
        {
            sparkService = SparkServiceFactory.build();

            storageService = StorageServiceFactory.build();
        }
        catch (ServiceException e)
        {
            LoggerFactory.getLogger(Manager.class).error("Can't instantiate services.");
        }
    }

}
