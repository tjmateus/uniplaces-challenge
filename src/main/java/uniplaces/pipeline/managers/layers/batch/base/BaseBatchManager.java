package uniplaces.pipeline.managers.layers.batch.base;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.services.database.DatabaseServiceFactory;
import uniplaces.pipeline.services.database.exceptions.DatabaseServiceException;
import uniplaces.pipeline.managers.exceptions.ManagerException;
import uniplaces.pipeline.managers.layers.batch.BatchManager;
import uniplaces.pipeline.managers.layers.batch.base.scheduler.BatchScheduler;
import uniplaces.pipeline.managers.layers.batch.exceptions.BatchManagerException;
import uniplaces.pipeline.model.Location;
import uniplaces.pipeline.model.View;
import uniplaces.pipeline.services.storage.exceptions.StorageServiceException;

import java.util.Set;
import java.util.stream.Collectors;


/**
 * Created by tmateus.
 */
public class BaseBatchManager extends BatchManager
{

    private static boolean BATCH_IS_PROCESSING = false;
    private static boolean BATCH_VIEW_IS_BEING_CREATED = false;

    public BaseBatchManager(Configuration batchConfiguration) throws ManagerException
    {
        super(batchConfiguration);

        BatchScheduler.start(batchConfiguration);
    }

    @Override
    public synchronized boolean createView() throws BatchManagerException
    {
        if(!BATCH_IS_PROCESSING)
        {
            BATCH_IS_PROCESSING = true;
            try
            {
                //Process each entity dataset

                //Email entity
                Location emailDatasetLocation =new Location(Location.Entity.ENTITY_EMAIL, Location.Fact.FACT_EVENT);
                Dataset<Row> emailDataset = this.getEntityDataset(emailDatasetLocation);

                //Name entity
                Location nameDatasetLocation =new Location(Location.Entity.ENTITY_NAME, Location.Fact.FACT_EVENT);
                Dataset<Row> nameDataset = this.getEntityDataset(nameDatasetLocation);

                //Merge the entities
                Dataset<Row> mergedDataset = sparkService.mergeEntities(emailDataset, nameDataset);

                //Persist view in database
                BATCH_VIEW_IS_BEING_CREATED = true;
                DatabaseServiceFactory.build().createView(View.BATCH_VIEW, mergedDataset);
                BATCH_VIEW_IS_BEING_CREATED = false;

                BATCH_IS_PROCESSING = false;

                return true;
            }
            catch (DatabaseServiceException | StorageServiceException e)
            {
                BATCH_VIEW_IS_BEING_CREATED = false;
                BATCH_IS_PROCESSING = false;
                throw new BatchManagerException(e.getMessage(), e);
            }
        }
        else
        {
            throw new BatchManagerException("A batch is already being processed.");
        }
    }

    @Override
    public synchronized Dataset<Row> getView() throws BatchManagerException
    {
        try
        {
            //Wait for the new batch view.
            while(BATCH_VIEW_IS_BEING_CREATED);

            return DatabaseServiceFactory.build().getView(sparkService.getJavaSparkContext(), View.BATCH_VIEW);
        }
        catch (DatabaseServiceException e)
        {
            throw new BatchManagerException(e.getMessage(), e);
        }
    }

    private Dataset<Row> getEntityDataset(Location location) throws StorageServiceException
    {
        //Get all files from hadoop
        Set<String> filesPath = storageService.getFilePaths()
                .stream()
                .filter(filePath -> filePath.contains(location.getEntity().toString()) && filePath.contains(location.getFact().toString()))
                .collect(Collectors.toSet());

        Dataset<Row> dataset = null;
        for(String filePath : filesPath)
        {
            //Process each file to get first register and last one for each guest
            Dataset<Row> datasetForFile = sparkService.processEntity(sparkService.readJson(filePath), location);
            if(dataset == null)
            {
                dataset = datasetForFile;
            }
            else
            {
                dataset = dataset.unionAll(datasetForFile);
            }
        }

        if(dataset != null)
        {
            //Merge all files and get the first register and last one for each guest
            return sparkService.processEntity(dataset, location);
        }

        return null;
    }

}
