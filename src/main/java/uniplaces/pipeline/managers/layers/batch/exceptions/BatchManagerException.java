package uniplaces.pipeline.managers.layers.batch.exceptions;


import uniplaces.pipeline.managers.exceptions.ManagerException;


/**
 * Created by tmateus.
 */
public class BatchManagerException extends ManagerException
{

    /* ATTRIBUTES ************************************************************/


    private static final long serialVersionUID = -4830513619374977007L;


    /* CONSTRUCTORS **********************************************************/


    public BatchManagerException()
    {
        super();
    }

    public BatchManagerException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public BatchManagerException(String message)
    {
        super(message);
    }

    public BatchManagerException(Throwable cause)
    {
        super(cause);
    }


    /* METHODS ***************************************************************/

    /* ACCESSORS/MODIFIERS ***************************************************/
}
