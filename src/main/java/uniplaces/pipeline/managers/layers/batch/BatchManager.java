package uniplaces.pipeline.managers.layers.batch;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.managers.Manager;
import uniplaces.pipeline.managers.exceptions.ManagerException;
import uniplaces.pipeline.managers.layers.batch.exceptions.BatchManagerException;


/**
 * Created by tmateus.
 */
public abstract class BatchManager extends Manager
{

    protected Configuration batchConfiguration;

    public BatchManager(Configuration batchConfiguration) throws ManagerException
    {
        this.batchConfiguration = batchConfiguration;
    }

    public abstract boolean createView() throws BatchManagerException;

    public abstract Dataset<Row> getView() throws BatchManagerException;

}
