package uniplaces.pipeline.managers.layers.batch;

import uniplaces.pipeline.configuration.ConfigurationFactory;
import uniplaces.pipeline.configuration.exceptions.ConfigurationException;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.managers.layers.batch.base.BaseBatchManager;
import uniplaces.pipeline.managers.exceptions.ManagerException;


/**
 * Created by tmateus.
 */
public class BatchManagerFactory
{

    private static final String CONFIGURATION_FILE_NAME = "config-pipeline-manager-batch.properties";

    private static final Configuration batchConfiguration;

    static
    {
        try
        {
            batchConfiguration = ConfigurationFactory.build(CONFIGURATION_FILE_NAME);
        }
        catch (ConfigurationException exception)
        {
            throw new RuntimeException(exception.getMessage(), exception);
        }
    }

    public static BatchManager build() throws ManagerException
    {
        return new BaseBatchManager(batchConfiguration);
    }

}
