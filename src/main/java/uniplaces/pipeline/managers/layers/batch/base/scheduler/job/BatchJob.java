package uniplaces.pipeline.managers.layers.batch.base.scheduler.job;

import org.quartz.*;
import org.slf4j.LoggerFactory;
import uniplaces.pipeline.managers.layers.batch.BatchManagerFactory;
import uniplaces.pipeline.managers.layers.batch.base.scheduler.model.BatchFrequency;

import java.util.Calendar;
import java.util.Date;


public class BatchJob implements Job {

    public static final String JOB_ID = "batch_job";

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException
    {
		try
        {
            LoggerFactory.getLogger(this.getClass().getSimpleName()).info("Initiated batch processEntity ["+new Date().toString()+"].");

            //Create batch view.
            BatchManagerFactory.build().createView();
    	}
        catch (Exception e)
        {
        	LoggerFactory.getLogger(this.getClass().getSimpleName()).error(e.getMessage(), e);
        }
	}
	
	public static TriggerBuilder<Trigger> getSyncTrigger(String id, BatchFrequency processorFrequency) throws SchedulerException
    {

        TriggerBuilder<Trigger> trigger = TriggerBuilder.newTrigger().withIdentity(id, id);

        //Create trigger, depending on frequency.
        switch (processorFrequency.getType())
        {
            case SECONDS:
                trigger.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(processorFrequency.getSecond()).repeatForever()).startNow();
                break;
            case MINUTES:
                trigger.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInMinutes(processorFrequency.getMinute()).repeatForever()).startNow();
                break;
            case HOURLY:
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.MINUTE, processorFrequency.getMinute());
                trigger.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInHours(1).repeatForever()).startAt(calendar.getTime());
                break;
            case DAILY:
                trigger.startNow().withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(processorFrequency.getHour(), processorFrequency.getMinute()));
                break;
            case WEEKLY:
                trigger.startNow().withSchedule(CronScheduleBuilder.weeklyOnDayAndHourAndMinute(BatchFrequency.mapDayOfWeekToInt.get(processorFrequency.getWeekDay()), processorFrequency.getHour(), processorFrequency.getMinute()));
                break;
            case MONTHLY:
                trigger.startNow().withSchedule(CronScheduleBuilder.monthlyOnDayAndHourAndMinute(processorFrequency.getDay(), processorFrequency.getHour(), processorFrequency.getMinute()));
                break;
        }

        return trigger;
    }
	
}
