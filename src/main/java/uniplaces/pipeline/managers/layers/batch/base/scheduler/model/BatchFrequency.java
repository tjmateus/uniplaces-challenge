package uniplaces.pipeline.managers.layers.batch.base.scheduler.model;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by tmateus.
 */
public class BatchFrequency
{
    private Type type;
    private Integer second;
    private Integer minute;
    private Integer hour;
    private Integer day;
    private WeekDay weekDay;

    public static enum Type
    {
        SECONDS, MINUTES, HOURLY, DAILY, WEEKLY, MONTHLY
    };

    public static enum WeekDay
    {
        Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
    }

    public static Map<WeekDay,Integer> mapDayOfWeekToInt = new HashMap<>();
    public static Map <Integer,WeekDay> mapIntToDayOfWeek = new HashMap <> ();

    static {
        mapDayOfWeekToInt.put(WeekDay.Sunday, 1);
        mapDayOfWeekToInt.put(WeekDay.Monday, 2);
        mapDayOfWeekToInt.put(WeekDay.Tuesday, 3);
        mapDayOfWeekToInt.put(WeekDay.Wednesday, 4);
        mapDayOfWeekToInt.put(WeekDay.Thursday, 5);
        mapDayOfWeekToInt.put(WeekDay.Friday, 6);
        mapDayOfWeekToInt.put(WeekDay.Saturday, 7);

        mapIntToDayOfWeek.put(1, WeekDay.Sunday);
        mapIntToDayOfWeek.put(2, WeekDay.Monday);
        mapIntToDayOfWeek.put(3, WeekDay.Tuesday);
        mapIntToDayOfWeek.put(4, WeekDay.Wednesday);
        mapIntToDayOfWeek.put(5, WeekDay.Thursday);
        mapIntToDayOfWeek.put(6, WeekDay.Friday);
        mapIntToDayOfWeek.put(7, WeekDay.Saturday);
    }

    public Type getType()
    {
        return type;
    }

    public void setType(Type type)
    {
        this.type = type;
    }

    public Integer getSecond()
    {
        return second;
    }

    public void setSecond(Integer second)
    {
        this.second = second;
    }

    public Integer getMinute()
    {
        return minute;
    }

    public void setMinute(Integer minute)
    {
        this.minute = minute;
    }

    public Integer getHour()
    {
        return hour;
    }

    public void setHour(Integer hour)
    {
        this.hour = hour;
    }

    public Integer getDay()
    {
        return day;
    }

    public void setDay(Integer day)
    {
        this.day = day;
    }

    public WeekDay getWeekDay()
    {
        return weekDay;
    }

    public void setWeekDay(WeekDay weekDay)
    {
        this.weekDay = weekDay;
    }

}