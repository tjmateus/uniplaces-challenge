package uniplaces.pipeline.managers.layers.batch.base.scheduler;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.LoggerFactory;
import uniplaces.pipeline.configuration.exceptions.ConfigurationException;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.managers.layers.batch.base.scheduler.job.BatchJob;
import uniplaces.pipeline.managers.layers.batch.base.scheduler.model.BatchFrequency;
import uniplaces.pipeline.managers.layers.batch.exceptions.BatchManagerException;


/**
 * Created by tmateus.
 */
public class BatchScheduler
{

    private static Scheduler scheduler;

    private static String PROPERTY_SCHEDULER_FREQUENCY_TYPE = "batch_sch_frequency_type";
    private static String PROPERTY_SCHEDULER_FREQUENCY_SECOND = "batch_sch_frequency_second";
    private static String PROPERTY_SCHEDULER_FREQUENCY_MINUTE = "batch_sch_frequency_minute";
    private static String PROPERTY_SCHEDULER_FREQUENCY_HOUR = "batch_sch_frequency_hour";
    private static String PROPERTY_SCHEDULER_FREQUENCY_WEEK_DAY = "batch_sch_frequency_week_day";
    private static String PROPERTY_SCHEDULER_FREQUENCY_DAY = "batch_sch_frequency_day";

    public static void start(Configuration configuration)
    {
        try
        {
            BatchFrequency batchFrequency = getBatchFrequency(configuration);

            //If a batch should happen
            if(batchFrequency.getType() != null)
            {
                //Create scheduler
                SchedulerFactory schFactory = new StdSchedulerFactory("quartz/batch.quartz.properties");
                scheduler = schFactory.getScheduler();
                scheduler.start();

                //Create processor job
                JobDetail job = JobBuilder.newJob(BatchJob.class).withIdentity(BatchJob.JOB_ID).build();

                //Create a new trigger
                TriggerBuilder<Trigger> trigger = BatchJob.getSyncTrigger(BatchJob.JOB_ID, batchFrequency);

                //Schedule job
                scheduler.scheduleJob(job, trigger.build());

                LoggerFactory.getLogger(BatchScheduler.class).info("Scheduled processor job.");
            }
        }
        catch (SchedulerException e)
        {
            LoggerFactory.getLogger(BatchScheduler.class).error("Could not schedule processor job.");
        }
        catch (BatchManagerException e)
        {
            LoggerFactory.getLogger(BatchScheduler.class).error("Could not schedule processor job. Please verify configuration file.");
        }
    }

    private static BatchFrequency getBatchFrequency(Configuration configuration) throws BatchManagerException
    {
        try
        {
            BatchFrequency batchFrequency = new BatchFrequency();

            //According to the type of frequency, the object is built.
            if(configuration.getProperty(PROPERTY_SCHEDULER_FREQUENCY_TYPE) != null && !configuration.getProperty(PROPERTY_SCHEDULER_FREQUENCY_TYPE).isEmpty())
            {
                BatchFrequency.Type frequencyType = BatchFrequency.Type.valueOf(configuration.getProperty(PROPERTY_SCHEDULER_FREQUENCY_TYPE));

                batchFrequency.setType(frequencyType);

                if(frequencyType.equals(BatchFrequency.Type.SECONDS))
                {
                    batchFrequency.setSecond(Integer.parseInt(configuration.getProperty(PROPERTY_SCHEDULER_FREQUENCY_SECOND)));
                }
                else
                {
                    batchFrequency.setMinute(Integer.parseInt(configuration.getProperty(PROPERTY_SCHEDULER_FREQUENCY_MINUTE)));
                }

                if (frequencyType.equals(BatchFrequency.Type.DAILY))
                {
                    batchFrequency.setHour(Integer.parseInt(configuration.getProperty(PROPERTY_SCHEDULER_FREQUENCY_HOUR)));
                }
                else if (frequencyType.equals(BatchFrequency.Type.WEEKLY))
                {
                    batchFrequency.setWeekDay(BatchFrequency.WeekDay.valueOf(configuration.getProperty(PROPERTY_SCHEDULER_FREQUENCY_WEEK_DAY)));
                }
                else if (frequencyType.equals(BatchFrequency.Type.MONTHLY))
                {
                    batchFrequency.setDay(Integer.parseInt(configuration.getProperty(PROPERTY_SCHEDULER_FREQUENCY_DAY)));
                }
            }

            return batchFrequency;
        }
        catch (ConfigurationException e)
        {
            throw new BatchManagerException(e.getMessage(), e);
        }
    }

}
