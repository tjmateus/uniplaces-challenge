package uniplaces.pipeline.managers.layers.serving;

import uniplaces.pipeline.configuration.ConfigurationFactory;
import uniplaces.pipeline.configuration.exceptions.ConfigurationException;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.managers.exceptions.ManagerException;
import uniplaces.pipeline.managers.layers.serving.base.BaseServingManager;


/**
 * Created by tmateus.
 */
public class ServingManagerFactory
{

    private static final String CONFIGURATION_FILE_NAME = "config-pipeline-manager-serving.properties";

    private static final Configuration servingConfiguration;

    static
    {
        try
        {
            servingConfiguration = ConfigurationFactory.build(CONFIGURATION_FILE_NAME);
        }
        catch (ConfigurationException exception)
        {
            throw new RuntimeException(exception.getMessage(), exception);
        }
    }

    public static ServingManager build() throws ManagerException
    {
        return new BaseServingManager(servingConfiguration);
    }

}
