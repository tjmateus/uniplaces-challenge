package uniplaces.pipeline.managers.layers.serving.base;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.functions;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.services.database.DatabaseServiceFactory;
import uniplaces.pipeline.services.database.exceptions.DatabaseServiceException;
import uniplaces.pipeline.managers.exceptions.ManagerException;
import uniplaces.pipeline.managers.layers.serving.ServingManager;
import uniplaces.pipeline.managers.layers.serving.exceptions.ServingManagerException;
import uniplaces.pipeline.model.Guest;
import uniplaces.pipeline.model.View;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by tmateus.
 */
public class BaseServingManager extends ServingManager
{

    public BaseServingManager(Configuration configuration) throws ManagerException
    {
        super(configuration);
    }

    @Override
    public List<Guest> getView(View view) throws ServingManagerException
    {
        //Get the merge between batch and speed views.
        if(view.equals(View.MERGE_VIEW))
        {
            return this.mergeBatchAndSpeedViews();
        }
        else
        {
            try
            {
                //Get only batch or speed view.
                Dataset<Row> dataFrame = DatabaseServiceFactory.build().getView(sparkService.getJavaSparkContext(), view);
                return this.convertDataFrameToGuest(dataFrame);
            }
            catch (DatabaseServiceException e)
            {
                throw new ServingManagerException(e.getMessage(), e);
            }
        }
    }

    private List<Guest> mergeBatchAndSpeedViews() throws ServingManagerException
    {
        try
        {
            //Get batch view.
            Dataset<Row> batchDataFrame = DatabaseServiceFactory.build().getView(sparkService.getJavaSparkContext(), View.BATCH_VIEW);

            //Get the speed view.
            Dataset<Row> speedDataFrame = DatabaseServiceFactory.build().getView(sparkService.getJavaSparkContext(), View.SPEED_VIEW);

            //Check if these views have records.
            long countBatchDataFrame = batchDataFrame.count();
            long countSpeedDataFrame = speedDataFrame.count();

            if(countBatchDataFrame == 0L && countSpeedDataFrame == 0L)
            {
                return new ArrayList<>();
            }
            else if(countBatchDataFrame == 0L)
            {
                //Only return speed view, because batch view is empty.
                return this.convertDataFrameToGuest(speedDataFrame);
            }
            else if (countSpeedDataFrame == 0L)
            {
                //Only return batch view, because speed view is empty.
                return this.convertDataFrameToGuest(batchDataFrame);
            }

            //Merge data between batch and speed views.
            Dataset<Row> mergedData = batchDataFrame
                    .join(speedDataFrame, batchDataFrame.col("id").equalTo(speedDataFrame.col("id")), "full_outer")
                    .select(functions.when(speedDataFrame.col("id").isNotNull(), speedDataFrame.col("id")).otherwise(batchDataFrame.col("id")).as("id")
                            , functions.when(speedDataFrame.col("name").isNotNull(), speedDataFrame.col("name")).otherwise(batchDataFrame.col("name")).as("name")
                            , functions.when(speedDataFrame.col("email").isNotNull(), speedDataFrame.col("email")).otherwise(batchDataFrame.col("email")).as("email")
                            , functions.when(speedDataFrame.col("created_at").isNotNull(), speedDataFrame.col("created_at")).otherwise(batchDataFrame.col("created_at")).as("created_at")
                            , functions.when(speedDataFrame.col("updated_at").isNotNull(), speedDataFrame.col("updated_at")).otherwise(batchDataFrame.col("updated_at")).as("updated_at")
                    )
                    .dropDuplicates();

            return this.convertDataFrameToGuest(mergedData);
        }
        catch (DatabaseServiceException e)
        {
            throw new ServingManagerException(e.getMessage(), e);
        }
    }

    //Convert spark datasets to a Guest model.
    private List<Guest> convertDataFrameToGuest(Dataset<Row> dataFrame)
    {
        List<Guest> guests = new ArrayList<>();

        Gson gson = new GsonBuilder()
                .setDateFormat("dd/MM/yyyy").create();

        Dataset<String> jsons = dataFrame.toJSON();

        for(String jsonString : jsons.collectAsList())
        {
            Guest guest = gson.fromJson(jsonString, Guest.class);

            guests.add(guest);
        }

        return guests;
    }
}
