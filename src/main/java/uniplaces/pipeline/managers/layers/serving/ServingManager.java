package uniplaces.pipeline.managers.layers.serving;

import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.managers.Manager;
import uniplaces.pipeline.managers.exceptions.ManagerException;
import uniplaces.pipeline.managers.layers.serving.exceptions.ServingManagerException;
import uniplaces.pipeline.model.Guest;
import uniplaces.pipeline.model.View;

import java.util.List;


/**
 * Created by tmateus.
 */
public abstract class ServingManager extends Manager
{

    private Configuration servingConfiguration;

    public ServingManager(Configuration servingConfiguration) throws ManagerException
    {
        this.servingConfiguration = servingConfiguration;
    }

    public abstract List<Guest> getView(View view) throws ServingManagerException;

}
