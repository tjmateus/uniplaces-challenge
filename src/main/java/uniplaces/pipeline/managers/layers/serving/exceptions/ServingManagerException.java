package uniplaces.pipeline.managers.layers.serving.exceptions;


import uniplaces.pipeline.managers.exceptions.ManagerException;


/**
 * Created by tmateus.
 */
public class ServingManagerException extends ManagerException
{

    /* ATTRIBUTES ************************************************************/


    private static final long serialVersionUID = -4830513619374977007L;


    /* CONSTRUCTORS **********************************************************/


    public ServingManagerException()
    {
        super();
    }

    public ServingManagerException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ServingManagerException(String message)
    {
        super(message);
    }

    public ServingManagerException(Throwable cause)
    {
        super(cause);
    }


    /* METHODS ***************************************************************/

    /* ACCESSORS/MODIFIERS ***************************************************/
}
