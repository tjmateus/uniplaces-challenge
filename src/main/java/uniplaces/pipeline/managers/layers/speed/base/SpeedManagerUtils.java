package uniplaces.pipeline.managers.layers.speed.base;

import com.google.gson.Gson;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.functions;
import org.slf4j.LoggerFactory;
import uniplaces.pipeline.datasource.exceptions.DataSourceException;
import uniplaces.pipeline.model.Event;
import uniplaces.pipeline.model.Location;

import java.io.*;


/**
 * Created by tmateus.
 */
public class SpeedManagerUtils implements Serializable
{

    private static final String SCHEMA_JSON_EVENT_VALIDATION = "/schema/schema-event.avsc";

    public static boolean isValidEvent(String eventJson) throws DataSourceException
    {
        try
        {
            //Use Apache Avro to check if the event is valid.
            InputStream input = new ByteArrayInputStream(eventJson.getBytes());
            DataInputStream din = new DataInputStream(input);

            Schema schema = new Schema.Parser().parse(SpeedManagerUtils.class.getResourceAsStream(SCHEMA_JSON_EVENT_VALIDATION));

            Decoder decoder = DecoderFactory.get().jsonDecoder(schema, din);

            GenericRecord event = new GenericData.Record(schema);

            DatumReader<GenericRecord> reader = new GenericDatumReader<>(schema);

            reader.read(event, decoder);

            return true;
        }
        catch(AvroRuntimeException e)
        {
            LoggerFactory.getLogger(SpeedManagerUtils.class).error("Couldn't processEntity event because it has an invalid format [" + eventJson + "].");
        }
        catch (IOException e)
        {
            throw new DataSourceException(e.getMessage(), e);
        }

        return false;
    }

    public static String buildAtomicEvents(String json, Location location) throws DataSourceException
    {
        Event atomicEvent = new Event();

        Gson gson = new Gson();
        Event event = gson.fromJson(json, Event.class);

        //If event has name entity
        if(location.getEntity().equals(Location.Entity.ENTITY_NAME) && event.getName() != null)
        {
            atomicEvent.setTimestamp(event.getTimestamp());
            atomicEvent.setGuestId(event.getGuestId());
            atomicEvent.setName(event.getName());
            return gson.toJson(atomicEvent);
        }
        //If event has email entity
        else if(location.getEntity().equals(Location.Entity.ENTITY_EMAIL) && event.getEmail() != null)
        {
            atomicEvent.setTimestamp(event.getTimestamp());
            atomicEvent.setGuestId(event.getGuestId());
            atomicEvent.setEmail(event.getEmail());
            return gson.toJson(atomicEvent);
        }
        else
        {
            LoggerFactory.getLogger(SpeedManagerUtils.class).error("Invalid Json " + json + " or Entity [" + location.getEntity().toString() + "].");
        }

        return "";

    }

    public static Column getFormattedDate(Column timestamp)
    {
        return functions.from_unixtime(timestamp, "dd/MM/yyyy");
    }

}
