package uniplaces.pipeline.managers.layers.speed;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.managers.Manager;
import uniplaces.pipeline.managers.exceptions.ManagerException;
import uniplaces.pipeline.managers.layers.speed.exceptions.SpeedManagerException;


/**
 * Created by tmateus.
 */
public abstract class SpeedManager extends Manager
{

    protected Logger logger;

    private Configuration speedConfiguration;

    public SpeedManager(Configuration speedConfiguration) throws ManagerException
    {
        this.speedConfiguration = speedConfiguration;

        logger = LoggerFactory.getLogger(SpeedManager.class);
    }

    public abstract void initStreamingListener() throws SpeedManagerException;

    public abstract Dataset<Row> getView() throws SpeedManagerException;

}
