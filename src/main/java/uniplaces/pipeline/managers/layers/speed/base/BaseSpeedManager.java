package uniplaces.pipeline.managers.layers.speed.base;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.StorageLevels;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.services.database.DatabaseServiceFactory;
import uniplaces.pipeline.services.database.exceptions.DatabaseServiceException;
import uniplaces.pipeline.managers.exceptions.ManagerException;
import uniplaces.pipeline.managers.layers.speed.SpeedManager;
import uniplaces.pipeline.managers.layers.speed.exceptions.SpeedManagerException;
import uniplaces.pipeline.model.Location;
import uniplaces.pipeline.model.View;
import uniplaces.pipeline.services.spark.exceptions.SparkServiceException;
import uniplaces.pipeline.services.storage.exceptions.StorageServiceException;

import java.util.List;


/**
 * Created by tmateus.
 */
public class BaseSpeedManager extends SpeedManager
{
    private static boolean SPEED_VIEW_IS_BEING_CREATED = false;

    private static boolean SPARK_LISTENING = false;

    public BaseSpeedManager(Configuration configuration) throws ManagerException
    {
        super(configuration);
    }

    @Override
    public void initStreamingListener() throws SpeedManagerException
    {

        if(!SPARK_LISTENING)
        {
            SPARK_LISTENING = true;

            try
            {
                JavaStreamingContext streamingContext = new JavaStreamingContext(sparkService.getSparkConf(), Durations.seconds(sparkService.getSparkStreamingDuration()));

                logger.info("Initiating streaming listener [" + sparkService.getSparkStreamingHost() + ":" + sparkService.getSparkStreamingPort() + "].");

                JavaReceiverInputDStream<String> eventStream = streamingContext.socketTextStream(sparkService.getSparkStreamingHost(), sparkService.getSparkStreamingPort(), StorageLevels.MEMORY_AND_DISK_SER);

                eventStream.foreachRDD((VoidFunction<JavaRDD<String>>) (JavaRDD<String> rdd) ->
                {
                    if (!rdd.isEmpty())
                    {
                        //Filter invalid events, using a schema enforcer
                        JavaRDD<String> eventsJson = rdd.filter((Function<String, Boolean>) SpeedManagerUtils::isValidEvent);

                        //Assure data atomicity and insert to storage

                        ////Name Entity
                        Location nameLocation = new Location(Location.Entity.ENTITY_NAME, Location.Fact.FACT_EVENT);
                        JavaRDD<String> nameEntityRDD = eventsJson.map((Function<String, String>) json -> SpeedManagerUtils.buildAtomicEvents(json, nameLocation)).filter(json -> !json.isEmpty());
                        new StorageWorker(nameEntityRDD.collect(), nameLocation).start();

                        ////Email Entity
                        Location emailLocation = new Location(Location.Entity.ENTITY_EMAIL, Location.Fact.FACT_EVENT);
                        JavaRDD<String> emailEntityRDD = eventsJson.map((Function<String, String>) json -> SpeedManagerUtils.buildAtomicEvents(json, emailLocation)).filter(json -> !json.isEmpty());
                        new StorageWorker(emailEntityRDD.collect(), emailLocation).start();

                        //Speed View
                        synchronized (BaseSpeedManager.class)
                        {
                            //Process name dataset
                            Dataset<Row> nameEntityDataFrame = sparkService.processEntity(sparkService.getSparkSession().read().json(nameEntityRDD), nameLocation);

                            //Process email dataset
                            Dataset<Row> emailEntityDataFrame = sparkService.processEntity(sparkService.getSparkSession().read().json(emailEntityRDD), emailLocation);

                            //Merge entities
                            Dataset<Row> mergedDataFrame = sparkService.mergeEntities(emailEntityDataFrame, nameEntityDataFrame);

                            //Create speed view.
                            this.createView(mergedDataFrame);
                        }
                    }
                });

                streamingContext.start();
                streamingContext.awaitTermination();
            }
            catch (InterruptedException | SparkServiceException e)
            {
                SPARK_LISTENING =false;
                throw new SpeedManagerException(e.getMessage(), e);
            }
        }
        else
        {
            throw new SpeedManagerException("Spark streaming has already started.");
        }

        SPARK_LISTENING =false;
    }

    private boolean createView(Dataset<Row> speedDataset) throws SpeedManagerException
    {
        try
        {
            //Persist view in database
            SPEED_VIEW_IS_BEING_CREATED = true;
            DatabaseServiceFactory.build().createView(View.SPEED_VIEW, speedDataset);
            SPEED_VIEW_IS_BEING_CREATED = false;
        }
        catch (DatabaseServiceException e)
        {
            SPEED_VIEW_IS_BEING_CREATED = false;
            throw new SpeedManagerException(e.getMessage(), e);
        }

        return true;
    }

    @Override
    public synchronized Dataset<Row> getView() throws SpeedManagerException
    {
        try
        {
            //Wait while the speed view is being created
            while(SPEED_VIEW_IS_BEING_CREATED);

            return DatabaseServiceFactory.build().getView(sparkService.getJavaSparkContext(), View.SPEED_VIEW);
        }
        catch (DatabaseServiceException e)
        {
            throw new SpeedManagerException(e.getMessage(), e);
        }
    }

    private class StorageWorker extends Thread
    {
        private List<String> events;
        private Location location;

        StorageWorker(List<String> events, Location location)
        {
            this.events = events;
            this.location = location;
        }

        public void run()
        {
            try
            {
                //Add atomic events to storage
                storageService.insertEvents(events, location);
            }
            catch (StorageServiceException e)
            {
                logger.error("Couldn't save events to storage. [" + events + "].");
            }
        }
    }
}
