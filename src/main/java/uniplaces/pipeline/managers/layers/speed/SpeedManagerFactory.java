package uniplaces.pipeline.managers.layers.speed;

import uniplaces.pipeline.configuration.ConfigurationFactory;
import uniplaces.pipeline.configuration.exceptions.ConfigurationException;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.managers.exceptions.ManagerException;
import uniplaces.pipeline.managers.layers.speed.base.BaseSpeedManager;


/**
 * Created by tmateus.
 */
public class SpeedManagerFactory
{

    private static final String CONFIGURATION_FILE_NAME = "config-pipeline-manager-batch.properties";

    private static final Configuration speedConfiguration;

    static
    {
        try
        {
            speedConfiguration = ConfigurationFactory.build(CONFIGURATION_FILE_NAME);
        }
        catch (ConfigurationException exception)
        {
            throw new RuntimeException(exception.getMessage(), exception);
        }
    }

    public static SpeedManager build() throws ManagerException
    {
        return new BaseSpeedManager(speedConfiguration);
    }

}
