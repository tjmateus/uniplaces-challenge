package uniplaces.pipeline.managers.layers.speed.exceptions;


import uniplaces.pipeline.managers.exceptions.ManagerException;


/**
 * Created by tmateus.
 */
public class SpeedManagerException extends ManagerException
{

    /* ATTRIBUTES ************************************************************/


    private static final long serialVersionUID = -4830513619374977007L;


    /* CONSTRUCTORS **********************************************************/


    public SpeedManagerException()
    {
        super();
    }

    public SpeedManagerException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public SpeedManagerException(String message)
    {
        super(message);
    }

    public SpeedManagerException(Throwable cause)
    {
        super(cause);
    }


    /* METHODS ***************************************************************/

    /* ACCESSORS/MODIFIERS ***************************************************/
}
