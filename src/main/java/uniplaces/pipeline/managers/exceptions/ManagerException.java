package uniplaces.pipeline.managers.exceptions;


import uniplaces.pipeline.exceptions.PipelineException;


/**
 * Created by tmateus.
 */
public class ManagerException extends PipelineException
{

    /* ATTRIBUTES ************************************************************/


    private static final long serialVersionUID = -4830513619374977007L;


    /* CONSTRUCTORS **********************************************************/


    public ManagerException()
    {
        super();
    }

    public ManagerException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ManagerException(String message)
    {
        super(message);
    }

    public ManagerException(Throwable cause)
    {
        super(cause);
    }


    /* METHODS ***************************************************************/

    /* ACCESSORS/MODIFIERS ***************************************************/
}
