/*
 * Copyright (c) 2009-2014 Wizdee, Lda.
 * All rights reserved.
 * 
 */


package uniplaces.pipeline.exceptions;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by tmateus.
 */
public abstract class PipelineException extends Exception
{

    private static final long serialVersionUID = 245684579222020733L;
    
    private Map<String, String> properties;
    
    
    public PipelineException()
    {
        super();

        this.properties = new HashMap<>();
    }
    

    public PipelineException(String message)
    {
        super(message);

        this.properties = new HashMap<>();
    }

    
    public PipelineException(Throwable cause)
    {
        super(cause);

        this.properties = getCauseProperties(cause);
    }

    
    public PipelineException(String message, Throwable cause)
    {
        super(message, cause);

        this.properties = getCauseProperties(cause);
    }


    public Map<String, String> getProperties()
    {
        return this.properties;
    }

    public Object getProperty(String key)
    {
        return this.properties.get(key);
    }

    public void setProperty(String key, String value)
    {
        this.properties.put(key, value);
    }

    private Map<String, String> getCauseProperties(Throwable cause)
    {
        if (cause == null)
        {
            return new HashMap<>();
        }

        if (cause instanceof PipelineException)
        {
            return ((PipelineException) cause).getProperties();
        }
        else
        {
            return getCauseProperties(cause.getCause());
        }
    }
}
