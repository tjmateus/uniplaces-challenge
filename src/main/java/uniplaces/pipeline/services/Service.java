package uniplaces.pipeline.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by tmateus.
 */
public abstract class Service
{

    protected final Logger logger;

    protected Service()
    {
        this.logger = LoggerFactory.getLogger(Service.class);
    }

}
