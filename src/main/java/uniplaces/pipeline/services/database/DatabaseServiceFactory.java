package uniplaces.pipeline.services.database;

import org.slf4j.LoggerFactory;
import uniplaces.pipeline.configuration.ConfigurationFactory;
import uniplaces.pipeline.configuration.exceptions.ConfigurationException;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.services.database.exceptions.DatabaseServiceException;
import uniplaces.pipeline.services.database.mongodb.MongoDBDatabaseService;

import java.lang.reflect.InvocationTargetException;


/**
 * Created by tmateus.
 */
public class DatabaseServiceFactory
{

    private static final String CONFIGURATION_FILE_NAME = "config-pipeline-service-database.properties";

    private static final String PROPERTY_CLASS = "db_class";

    private static Class<? extends DatabaseService> databaseClass;

    private static final Configuration databaseConfiguration;

    static
    {
        databaseClass = MongoDBDatabaseService.class;

        try
        {
            databaseConfiguration = ConfigurationFactory.build(CONFIGURATION_FILE_NAME);

            String managerClassName = databaseConfiguration.getProperty(PROPERTY_CLASS, null);
            if (managerClassName != null && managerClassName.length() > 0)
            {
                databaseClass = (Class<? extends DatabaseService>) Class.forName(managerClassName);
            }
        }
        catch (ConfigurationException | ClassNotFoundException exception)
        {
            throw new RuntimeException(exception.getMessage(), exception);
        }

        LoggerFactory.getLogger(DatabaseServiceFactory.class).info("Building instances of type [" + databaseClass.getName() + "].");
    }

    public static DatabaseService build() throws DatabaseServiceException
    {
        try
        {
            return databaseClass.getDeclaredConstructor(Configuration.class).newInstance(databaseConfiguration);
        }
        catch (IllegalAccessException | InstantiationException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException exception)
        {
            throw new DatabaseServiceException(exception.getMessage(), exception);
        }
    }

}
