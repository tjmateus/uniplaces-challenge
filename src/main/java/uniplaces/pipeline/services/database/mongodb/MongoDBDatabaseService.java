package uniplaces.pipeline.services.database.mongodb;

import com.mongodb.MongoClient;
import com.mongodb.spark.MongoSpark;
import com.mongodb.spark.config.ReadConfig;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.services.database.DatabaseService;
import uniplaces.pipeline.services.database.exceptions.DatabaseServiceException;
import uniplaces.pipeline.model.View;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by tmateus.
 */
public class MongoDBDatabaseService extends DatabaseService
{

    private MongoClient mongoClient;

    public MongoDBDatabaseService(Configuration databaseConfiguration) throws DatabaseServiceException
    {
        super(databaseConfiguration);

        if(mongoClient != null)
        {
            mongoClient = new MongoClient(this.getDatabaseHost(), this.getDatabasePort());
        }
    }

    @Override
    public synchronized boolean createView(View view, Dataset<Row> dataFrame)
    {
        //Write to MongoDB
        MongoSpark.write(dataFrame).option("collection", view.toString()).mode("overwrite").save();
        return true;
    }

    @Override
    public boolean cleanView(View view) throws DatabaseServiceException
    {
        //Clean collection
        mongoClient.getDatabase(this.getDatabaseName()).getCollection(view.toString()).drop();

        return true;
    }

    @Override
    public Dataset<Row> getView(JavaSparkContext jsc, View view)
    {
        // Read configuration
        Map<String, String> readOverrides = new HashMap<>();
        readOverrides.put("collection", view.toString());
        ReadConfig readConfig = ReadConfig.create(jsc).withOptions(readOverrides);

        //Get the view with Spark format
        return MongoSpark.load(jsc, readConfig).toDF();
    }

}
