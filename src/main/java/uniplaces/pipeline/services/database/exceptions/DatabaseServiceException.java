package uniplaces.pipeline.services.database.exceptions;


import uniplaces.pipeline.services.exceptions.ServiceException;


/**
 * Created by tmateus.
 */
public class DatabaseServiceException extends ServiceException
{

    /* ATTRIBUTES ************************************************************/


    private static final long serialVersionUID = -4830513619374977007L;


    /* CONSTRUCTORS **********************************************************/


    public DatabaseServiceException()
    {
        super();
    }

    public DatabaseServiceException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public DatabaseServiceException(String message)
    {
        super(message);
    }

    public DatabaseServiceException(Throwable cause)
    {
        super(cause);
    }


    /* METHODS ***************************************************************/

    /* ACCESSORS/MODIFIERS ***************************************************/
}
