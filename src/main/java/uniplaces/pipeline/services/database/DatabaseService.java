package uniplaces.pipeline.services.database;


import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import uniplaces.pipeline.configuration.exceptions.ConfigurationException;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.model.View;
import uniplaces.pipeline.services.Service;
import uniplaces.pipeline.services.database.exceptions.DatabaseServiceException;


/**
 * Created by tmateus.
 */
public abstract class DatabaseService extends Service
{

    private final static String PROPERTY_HOST = "db_host";
    private final static String PROPERTY_PORT = "db_port";
    private final static String PROPERTY_NAME = "db_name";
    private final static String PROPERTY_USERNAME = "db_username";
    private final static String PROPERTY_PASSWORD = "db_password";

    private final Configuration databaseConfiguration;

    public DatabaseService(Configuration databaseConfiguration) throws DatabaseServiceException
    {
        this.databaseConfiguration = databaseConfiguration;
    }

    public abstract boolean createView(View view, Dataset<Row> dataFrame);

    public abstract boolean cleanView(View view) throws DatabaseServiceException;

    public abstract Dataset<Row> getView(JavaSparkContext jsc, View view);

    protected String getDatabaseHost() throws DatabaseServiceException
    {
        try
        {
            return this.databaseConfiguration.getProperty(PROPERTY_HOST);
        }
        catch (ConfigurationException e)
        {
            throw new DatabaseServiceException("Couldn't get database host.");
        }
    }

    protected Integer getDatabasePort() throws DatabaseServiceException
    {
        try
        {
            return Integer.parseInt(this.databaseConfiguration.getProperty(PROPERTY_PORT));
        }
        catch (ConfigurationException e)
        {
            throw new DatabaseServiceException("Couldn't get database port.");
        }
    }

    protected String getDatabaseName() throws DatabaseServiceException
    {
        try
        {
            return this.databaseConfiguration.getProperty(PROPERTY_NAME);
        }
        catch (ConfigurationException e)
        {
            throw new DatabaseServiceException("Couldn't get database name.");
        }
    }

    protected String getDatabaseUsername() throws DatabaseServiceException
    {
        try
        {
            return this.databaseConfiguration.getProperty(PROPERTY_USERNAME);
        }
        catch (ConfigurationException e)
        {
            throw new DatabaseServiceException("Couldn't get database username.");
        }
    }

    protected String getDatabasePassword() throws DatabaseServiceException
    {
        try
        {
            return this.databaseConfiguration.getProperty(PROPERTY_PASSWORD);
        }
        catch (ConfigurationException e)
        {
            throw new DatabaseServiceException("Couldn't get database password.");
        }
    }

}
