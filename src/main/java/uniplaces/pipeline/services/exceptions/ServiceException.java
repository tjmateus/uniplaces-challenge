package uniplaces.pipeline.services.exceptions;


import uniplaces.pipeline.exceptions.PipelineException;


/**
 * Created by tmateus.
 */
public class ServiceException extends PipelineException
{

    /* ATTRIBUTES ************************************************************/


    private static final long serialVersionUID = -4830513619374977007L;


    /* CONSTRUCTORS **********************************************************/


    public ServiceException()
    {
        super();
    }

    public ServiceException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ServiceException(String message)
    {
        super(message);
    }

    public ServiceException(Throwable cause)
    {
        super(cause);
    }


    /* METHODS ***************************************************************/

    /* ACCESSORS/MODIFIERS ***************************************************/
}
