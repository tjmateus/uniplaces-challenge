package uniplaces.pipeline.services.storage;

import org.slf4j.LoggerFactory;
import uniplaces.pipeline.configuration.ConfigurationFactory;
import uniplaces.pipeline.configuration.exceptions.ConfigurationException;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.services.storage.exceptions.StorageServiceException;
import uniplaces.pipeline.services.storage.hadoop.HadoopStorageService;

import java.lang.reflect.InvocationTargetException;


/**
 * Created by tmateus.
 */
public class StorageServiceFactory
{

    private static final String CONFIGURATION_FILE_NAME = "config-pipeline-service-storage.properties";

    private static final String PROPERTY_CLASS = "storage_class";

    private static Class<? extends StorageService> storageClass;

    private static final Configuration storageConfiguration;

    static
    {
        storageClass = HadoopStorageService.class;

        try
        {
            storageConfiguration = ConfigurationFactory.build(CONFIGURATION_FILE_NAME);

            String managerClassName = storageConfiguration.getProperty(PROPERTY_CLASS, null);
            if (managerClassName != null && managerClassName.length() > 0)
            {
                storageClass = (Class<? extends StorageService>) Class.forName(managerClassName);
            }
        }
        catch (ConfigurationException | ClassNotFoundException exception)
        {
            throw new RuntimeException(exception.getMessage(), exception);
        }

        LoggerFactory.getLogger(StorageServiceFactory.class).info("Building instances of type [" + storageClass.getName() + "].");
    }

    public static StorageService build() throws StorageServiceException
    {
        try
        {
            return storageClass.getDeclaredConstructor(Configuration.class).newInstance(storageConfiguration);
        }
        catch (IllegalAccessException | InstantiationException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException exception)
        {
            throw new StorageServiceException(exception.getMessage(), exception);
        }
    }

}
