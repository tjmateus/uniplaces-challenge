package uniplaces.pipeline.services.storage.hadoop;

import org.apache.hadoop.fs.*;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.model.Location;
import uniplaces.pipeline.services.storage.StorageService;
import uniplaces.pipeline.services.storage.exceptions.StorageServiceException;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by tmateus.
 */
public class HadoopStorageService extends StorageService
{
    private final org.apache.hadoop.conf.Configuration hadoopConfiguration;

    public HadoopStorageService(Configuration configuration) throws StorageServiceException
    {
        super(configuration);

        hadoopConfiguration = new org.apache.hadoop.conf.Configuration();
        hadoopConfiguration.set("fs.defaultFS", this.getConnectionUrl());
    }

    @Override
    public boolean insertEvents(List<String> events, Location location) throws StorageServiceException
    {
        return this.insertEvent(String.join("\n", events), location);
    }

    @Override
    public synchronized boolean insertEvent(String event, Location location) throws StorageServiceException
    {
        try
        {
            logger.info("Connecting to " + hadoopConfiguration.get("fs.defaultFS") + ".");

            FileSystem fs = FileSystem.get(hadoopConfiguration);

            Path outputFile = new Path(this.getFilePath(location));

            //Prevent new write until file is ready.
            fs.setReplication(outputFile, (short) 1);

            FSDataOutputStream outputStream;

            // Check if it should create a new file or append to a file.
            if (fs.exists(outputFile))
            {
                logger.info("Appending file...");
                outputStream = fs.append(outputFile);
            }
            else
            {
                logger.info("Creating file...");
                outputStream = fs.create(outputFile);
            }

            event += "\n";
            outputStream.write(event.getBytes());

            outputStream.close();
            outputStream.flush();

            fs.close();

            logger.info("File written to storage [" + this.getFilePath(location) + "].");
        }
        catch (IOException e)
        {
            throw new StorageServiceException(e.getMessage(), e);
        }

        return true;
    }

    @Override
    public Set<String> getFilePaths() throws StorageServiceException
    {
        try
        {
            Set<String> filePaths = new HashSet<>();

            FileSystem fileSystem = FileSystem.get(hadoopConfiguration);

            Path filePath = new Path(this.getConnectionDirectory());

            FileStatus[] fileStatus = fileSystem.listStatus(filePath);

            getFilePaths(fileSystem, fileStatus, filePaths);

            return filePaths;
        }
        catch (IOException e)
        {
            throw new StorageServiceException(e.getMessage(), e);
        }
    }

    private FileStatus getFilePaths(FileSystem fileSystem, FileStatus[] fileStatusList, Set<String> filePaths) throws IOException
    {
        if(fileStatusList != null)
        {
            Path[] paths = FileUtil.stat2Paths(fileStatusList);

            for(Path path : paths)
            {
                if(path.toString().endsWith(".json"))
                {
                    filePaths.add(path.toString());
                }
                else
                {
                    getFilePaths(fileSystem, fileSystem.listStatus(path), filePaths);
                }
            }
        }

        return null;
    }

}
