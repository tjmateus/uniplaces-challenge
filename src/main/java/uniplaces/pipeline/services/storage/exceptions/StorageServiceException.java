package uniplaces.pipeline.services.storage.exceptions;


import uniplaces.pipeline.services.exceptions.ServiceException;


/**
 * Created by tmateus.
 */
public class StorageServiceException extends ServiceException
{

    /* ATTRIBUTES ************************************************************/


    private static final long serialVersionUID = -4830513619374977007L;


    /* CONSTRUCTORS **********************************************************/


    public StorageServiceException()
    {
        super();
    }

    public StorageServiceException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public StorageServiceException(String message)
    {
        super(message);
    }

    public StorageServiceException(Throwable cause)
    {
        super(cause);
    }


    /* METHODS ***************************************************************/

    /* ACCESSORS/MODIFIERS ***************************************************/
}
