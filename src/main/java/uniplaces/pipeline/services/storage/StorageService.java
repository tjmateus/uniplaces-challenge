package uniplaces.pipeline.services.storage;


import uniplaces.pipeline.configuration.exceptions.ConfigurationException;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.model.Location;
import uniplaces.pipeline.services.Service;
import uniplaces.pipeline.services.storage.exceptions.StorageServiceException;

import java.util.List;
import java.util.Set;


/**
 * Created by tmateus.
 */
public abstract class StorageService extends Service
{
    private static final String PROPERTY_CONNECTION_URL = "storage_connection_url";
    private static final String PROPERTY_DIRECTORY = "storage_directory";

    private Configuration configuration;

    public StorageService(Configuration configuration)
    {
        this.configuration = configuration;
    }

    public abstract boolean insertEvents(List<String> events, Location location) throws StorageServiceException;

    public abstract boolean insertEvent(String event, Location location) throws StorageServiceException;

    public String getFilePath(Location location) throws StorageServiceException
    {
        return this.getConnectionUrl() + this.getDirectory() + location.getFilePath();
    }

    public abstract Set<String> getFilePaths() throws StorageServiceException;

    protected String getConnectionDirectory() throws StorageServiceException
    {
        return this.getConnectionUrl() + this.getDirectory();
    }

    protected String getConnectionUrl() throws StorageServiceException
    {
        try
        {
            return this.configuration.getProperty(PROPERTY_CONNECTION_URL);
        }
        catch (ConfigurationException e)
        {
            throw new StorageServiceException("Couldn't get connection URL.");
        }
    }

    protected String getDirectory() throws StorageServiceException
    {
        try
        {
            return this.configuration.getProperty(PROPERTY_DIRECTORY);
        }
        catch (ConfigurationException e)
        {
            throw new StorageServiceException("Couldn't get directory.");
        }
    }

}
