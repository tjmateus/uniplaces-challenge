package uniplaces.pipeline.services.spark.exceptions;


import uniplaces.pipeline.services.exceptions.ServiceException;


/**
 * Created by tmateus.
 */
public class SparkServiceException extends ServiceException
{

    /* ATTRIBUTES ************************************************************/


    private static final long serialVersionUID = -4830513619374977007L;


    /* CONSTRUCTORS **********************************************************/


    public SparkServiceException()
    {
        super();
    }

    public SparkServiceException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public SparkServiceException(String message)
    {
        super(message);
    }

    public SparkServiceException(Throwable cause)
    {
        super(cause);
    }


    /* METHODS ***************************************************************/

    /* ACCESSORS/MODIFIERS ***************************************************/
}
