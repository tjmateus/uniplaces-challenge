package uniplaces.pipeline.services.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import uniplaces.pipeline.configuration.exceptions.ConfigurationException;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.model.Location;
import uniplaces.pipeline.services.Service;
import uniplaces.pipeline.services.exceptions.ServiceException;
import uniplaces.pipeline.services.spark.exceptions.SparkServiceException;


/**
 * Created by tmateus.
 */
public abstract class SparkService extends Service
{

    private static final String PROPERTY_SPARK_APP_NAME = "spark_app_name";
    private static final String PROPERTY_SPARK_MASTER = "spark_master";
    private static final String PROPERTY_SPARK_MONGODB_INPUT_URI = "spark_mongodb_input_uri";
    private static final String PROPERTY_SPARK_MONGODB_OUTPUT_URI = "spark_mongodb_output_uri";

    private static final String PROPERTY_SPARK_STREAMING_DURATION = "spark_streaming_duration";
    private static final String PROPERTY_SPARK_STREAMING_HOST = "spark_streaming_host";
    private static final String PROPERTY_SPARK_STREAMING_PORT = "spark_streaming_port";

    private SparkConf sparkConf;
    private SparkSession sparkSession;
    private JavaSparkContext javaSparkContext;

    private final Configuration sparkServiceConfiguration;

    public SparkService(Configuration sparkServiceConfiguration) throws ServiceException
    {
        this.sparkServiceConfiguration = sparkServiceConfiguration;

        this.sparkConf = this.buildSparkConf();
        this.sparkSession = this.buildSparkSession();
        this.javaSparkContext = this.buildJavaSparkContext();
    }

    public abstract Dataset<Row> readJson(String filePath);

    public abstract Dataset<Row> processEntity(Dataset<Row> dataset, Location location);

    public abstract Dataset<Row> mergeEntities(Dataset<Row> emailDataset, Dataset<Row> nameDataset);

    public SparkConf getSparkConf()
    {
        return sparkConf;
    }

    public SparkSession getSparkSession()
    {
        return sparkSession;
    }

    public JavaSparkContext getJavaSparkContext()
    {
        return javaSparkContext;
    }

    public Integer getSparkStreamingDuration() throws SparkServiceException
    {
        try
        {
            return Integer.parseInt(this.sparkServiceConfiguration.getProperty(PROPERTY_SPARK_STREAMING_DURATION, "10"));
        }
        catch (ConfigurationException e)
        {
            throw new SparkServiceException("Couldn't get spark streaming duration.");
        }
    }

    public String getSparkStreamingHost() throws SparkServiceException
    {
        try
        {
            return this.sparkServiceConfiguration.getProperty(PROPERTY_SPARK_STREAMING_HOST, "localhost");
        }
        catch (ConfigurationException e)
        {
            throw new SparkServiceException("Couldn't get spark streaming host.");
        }
    }

    public Integer getSparkStreamingPort() throws SparkServiceException
    {
        try
        {
            return Integer.parseInt(this.sparkServiceConfiguration.getProperty(PROPERTY_SPARK_STREAMING_PORT, "6789"));
        }
        catch (ConfigurationException e)
        {
            throw new SparkServiceException("Couldn't get spark streaming port.");
        }
    }

    private SparkConf buildSparkConf() throws SparkServiceException
    {
        SparkConf sparkConf = new SparkConf();
        sparkConf.setAppName(this.getSparkAppName());
        sparkConf.setMaster(this.getSparkMaster());
        sparkConf.set("spark.driver.allowMultipleContexts", "true");

        if(this.getSparkMongoDBInputUri() != null)
        {
            sparkConf.set("spark.mongodb.input.uri", this.getSparkMongoDBInputUri());
        }

        if(this.getSparkMongoDBOutputUri() != null)
        {
            sparkConf.set("spark.mongodb.output.uri", this.getSparkMongoDBOutputUri());
        }

        return sparkConf;
    }

    private SparkSession buildSparkSession() throws SparkServiceException
    {
        return SparkSession.builder().config(sparkConf).getOrCreate();
    }

    private JavaSparkContext buildJavaSparkContext() throws SparkServiceException
    {
        return JavaSparkContext.fromSparkContext(this.sparkSession.sparkContext());
    }

    private String getSparkAppName() throws SparkServiceException
    {
        try
        {
            return this.sparkServiceConfiguration.getProperty(PROPERTY_SPARK_APP_NAME, null);
        }
        catch (ConfigurationException e)
        {
            throw new SparkServiceException("Couldn't get spark app name.");
        }
    }

    private String getSparkMaster() throws SparkServiceException
    {
        try
        {
            return this.sparkServiceConfiguration.getProperty(PROPERTY_SPARK_MASTER, null);
        }
        catch (ConfigurationException e)
        {
            throw new SparkServiceException("Couldn't get spark master.");
        }
    }

    private String getSparkMongoDBInputUri() throws SparkServiceException
    {
        try
        {
            return this.sparkServiceConfiguration.getProperty(PROPERTY_SPARK_MONGODB_INPUT_URI, null);
        }
        catch (ConfigurationException e)
        {
            throw new SparkServiceException("Couldn't get spark Mongo DB input URI.");
        }
    }

    private String getSparkMongoDBOutputUri() throws SparkServiceException
    {
        try
        {
            return this.sparkServiceConfiguration.getProperty(PROPERTY_SPARK_MONGODB_OUTPUT_URI, null);
        }
        catch (ConfigurationException e)
        {
            throw new SparkServiceException("Couldn't get spark Mongo DB output URI.");
        }
    }

}
