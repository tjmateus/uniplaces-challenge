package uniplaces.pipeline.services.spark;

import uniplaces.pipeline.configuration.ConfigurationFactory;
import uniplaces.pipeline.configuration.exceptions.ConfigurationException;
import uniplaces.pipeline.services.exceptions.ServiceException;
import uniplaces.pipeline.services.spark.base.BaseSparkService;


/**
 * Created by tmateus.
 */
public class SparkServiceFactory
{

    private static final String CONFIGURATION_SPARK_SQL_FILE_NAME = "config-pipeline-service-spark.properties";

    public static SparkService build() throws ServiceException
    {
        try
        {
            return new BaseSparkService(ConfigurationFactory.build(CONFIGURATION_SPARK_SQL_FILE_NAME));
        }
            catch (ConfigurationException exception)
        {
            throw new RuntimeException(exception.getMessage(), exception);
        }
    }

}
