package uniplaces.pipeline.services.spark.base;

import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.functions;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.managers.layers.speed.base.SpeedManagerUtils;
import uniplaces.pipeline.model.Location;
import uniplaces.pipeline.services.exceptions.ServiceException;
import uniplaces.pipeline.services.spark.SparkService;


/**
 * Created by tmateus.
 */
public class BaseSparkService extends SparkService
{

    public BaseSparkService(Configuration configuration) throws ServiceException
    {
        super(configuration);
    }

    @Override
    public Dataset<Row> readJson(String filePath)
    {
        return this.getSparkSession().read().json(filePath);
    }

    @Override
    public Dataset<Row> processEntity(Dataset<Row> dataset, Location location)
    {
        //Get entity and fact from location
        String column = location.getEntity().toString();

        if(dataset == null || dataset.count() == 0L)
        {
            return null;
        }

        //Convert timestamp from string to timestamp type.
        Column unixTimestamp = functions.unix_timestamp(dataset.col("timestamp"), "d/M/yyyy");

        //Get the first events to get created date.
        Dataset<Row> firstEvents = dataset
                .groupBy(dataset.col("guest_id"))
                .agg(functions.min(unixTimestamp).as("created_at"))
                .select("guest_id", "created_at");

        //Get the last events to get updated date.
        Dataset<Row> lastEvents = dataset.
                groupBy(dataset.col("guest_id"))
                .agg(functions.max(unixTimestamp).as("updated_at"))
                .select("guest_id", "updated_at");

        //Join first events with last ones.
        Dataset<Row> joinFirstAndLastEvents = firstEvents
                .join(lastEvents, dataset.col("guest_id").equalTo(lastEvents.col("guest_id"))).as("last_event")
                .select(firstEvents.col("guest_id"), firstEvents.col("created_at"), lastEvents.col("updated_at"));

        //Join the dataset with the registers of first and last events.
        return dataset
                .join(joinFirstAndLastEvents, dataset.col("guest_id").equalTo(joinFirstAndLastEvents.col("guest_id")))
                .where(unixTimestamp.equalTo(joinFirstAndLastEvents.col("updated_at")))
                .select(dataset.col("guest_id").as("id"), dataset.col(column), joinFirstAndLastEvents.col("created_at"), joinFirstAndLastEvents.col("updated_at"))
                .dropDuplicates();
    }

    @Override
    public Dataset<Row> mergeEntities(Dataset<Row> emailDataset, Dataset<Row> nameDataset)
    {
        //Should add all columns. e.g. if name doesn't exist, it should add it with null value.
        if(emailDataset == null && nameDataset == null)
        {
            return null;
        }
        else if(emailDataset == null)
        {
            //Get name entity, but with others entities with null value -> easier to merge speed view and batch view.
            Dataset<Row> dataset =  nameDataset
                    .select(nameDataset.col("id"), nameDataset.col("name"),
                            SpeedManagerUtils.getFormattedDate(nameDataset.col("created_at")).as("created_at"),
                            SpeedManagerUtils.getFormattedDate(nameDataset.col("updated_at")).as("updated_at"))
                    .withColumn("email", functions.lit(null));

            return dataset;
        }
        else if(nameDataset == null)
        {
            //Get email entity, but with others entities with null value -> easier to merge speed view and batch view.
            Dataset<Row> dataset = emailDataset
                    .select(emailDataset.col("id"), emailDataset.col("email"),
                            SpeedManagerUtils.getFormattedDate(emailDataset.col("created_at")).as("created_at"),
                            SpeedManagerUtils.getFormattedDate(emailDataset.col("updated_at")).as("updated_at"))
                    .withColumn("name", functions.lit(null));

            return dataset;
        }

        //Merge entities.
        Dataset<Row> dataset =  emailDataset.join(nameDataset,  emailDataset.col("id").equalTo(nameDataset.col("id")))
                .select(emailDataset.col("id"), nameDataset.col("name"), emailDataset.col("email"),
                        functions.when(nameDataset.col("created_at").lt(emailDataset.col("created_at")), SpeedManagerUtils.getFormattedDate(nameDataset.col("created_at"))).otherwise(SpeedManagerUtils.getFormattedDate(emailDataset.col("created_at"))).as("created_at"),
                        functions.when(nameDataset.col("updated_at").gt(emailDataset.col("updated_at")), SpeedManagerUtils.getFormattedDate(nameDataset.col("updated_at"))).otherwise(SpeedManagerUtils.getFormattedDate(emailDataset.col("updated_at"))).as("updated_at"));

        return dataset;
    }

}
