package uniplaces.pipeline.datasource.base;

import com.google.gson.Gson;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.datasource.DataSource;
import uniplaces.pipeline.datasource.exceptions.DataSourceException;
import uniplaces.pipeline.model.Event;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Created by tmateus.
 */
public class BaseDataSource extends DataSource
{

    public BaseDataSource(Configuration dataSourceConfiguration) throws DataSourceException
    {
        super(dataSourceConfiguration);
    }

    @Override
    public boolean insertEvent(Event event) throws DataSourceException
    {
        //convert event to json
        String eventJson = new Gson().toJson(event);

        //write event to socket
        this.writeToSocket(new ArrayList<>(Collections.singleton(eventJson)));

        return true;
    }

    @Override
    public boolean insertEvents(List<Event> events) throws DataSourceException
    {
        List<String> eventsJson = new ArrayList<>();

        //Convert all the events to json
        for(Event event : events)
        {
            eventsJson.add(new Gson().toJson(event));
        }

        //write to socket
        this.writeToSocket(eventsJson);

        return true;
    }

    private void writeToSocket(List<String> eventsJson) throws DataSourceException
    {
        try
        {
            //open output stream to socket
            Socket clientSocket = new Socket(this.getSocketHost(), this.getSocketPort());
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());

            //Write event to socket
            outToServer.writeBytes(String.join("\n", eventsJson) + '\n');
            clientSocket.close();
        }
        catch (IOException e)
        {
            throw new DataSourceException(e.getMessage(), e);
        }
    }
}
