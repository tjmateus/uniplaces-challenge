package uniplaces.pipeline.datasource;

import uniplaces.pipeline.configuration.ConfigurationFactory;
import uniplaces.pipeline.configuration.exceptions.ConfigurationException;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.datasource.base.BaseDataSource;
import uniplaces.pipeline.datasource.exceptions.DataSourceException;


/**
 * Created by tmateus.
 */
public class DataSourceFactory
{

    private static final String CONFIGURATION_FILE_NAME = "config-pipeline-service-spark.properties";

    private static final Configuration dataSourceConfiguration;

    static
    {
        try
        {
            dataSourceConfiguration = ConfigurationFactory.build(CONFIGURATION_FILE_NAME);
        }
        catch (ConfigurationException exception)
        {
            throw new RuntimeException(exception.getMessage(), exception);
        }
    }

    public static DataSource build() throws DataSourceException
    {
        return new BaseDataSource(dataSourceConfiguration);
    }

}
