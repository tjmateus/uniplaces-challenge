package uniplaces.pipeline.datasource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uniplaces.pipeline.configuration.exceptions.ConfigurationException;
import uniplaces.pipeline.configuration.model.Configuration;
import uniplaces.pipeline.datasource.exceptions.DataSourceException;
import uniplaces.pipeline.model.Event;

import java.util.List;


/**
 * Created by tmateus.
 */
public abstract class DataSource
{

    protected Configuration dataSourceConfiguration;

    private static final String PROPERTY_DATASOURCE_SOCKET_HOST = "datasource_socket_host";
    private static final String PROPERTY_DATASOURCE_SOCKET_PORT = "datasource_socket_port";

    protected Logger logger;

    public DataSource(Configuration dataSourceConfiguration) throws DataSourceException
    {
        this.dataSourceConfiguration = dataSourceConfiguration;

        logger = LoggerFactory.getLogger(DataSource.class);
    }

    public abstract boolean insertEvent(Event event) throws DataSourceException;

    public abstract boolean insertEvents(List<Event> events) throws DataSourceException;

    protected String getSocketHost() throws DataSourceException
    {
        try
        {
            return this.dataSourceConfiguration.getProperty(PROPERTY_DATASOURCE_SOCKET_HOST, "localhost");
        }
        catch (ConfigurationException e)
        {
            throw new DataSourceException("Couldn't get socket host.");
        }
    }

    protected Integer getSocketPort() throws DataSourceException
    {
        try
        {
            return Integer.parseInt(this.dataSourceConfiguration.getProperty(PROPERTY_DATASOURCE_SOCKET_PORT, "6789"));
        }
        catch (ConfigurationException e)
        {
            throw new DataSourceException("Couldn't get socket port.");
        }
    }

}
