package uniplaces.pipeline.datasource.exceptions;


import uniplaces.pipeline.exceptions.PipelineException;


/**
 * Created by tmateus.
 */
public class DataSourceException extends PipelineException
{

    /* ATTRIBUTES ************************************************************/


    private static final long serialVersionUID = -4830513619374977007L;


    /* CONSTRUCTORS **********************************************************/


    public DataSourceException()
    {
        super();
    }

    public DataSourceException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public DataSourceException(String message)
    {
        super(message);
    }

    public DataSourceException(Throwable cause)
    {
        super(cause);
    }


    /* METHODS ***************************************************************/

    /* ACCESSORS/MODIFIERS ***************************************************/
}
