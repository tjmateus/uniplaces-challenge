package uniplaces.pipeline.model;

/**
 * Created by tmateus.
 */
public enum View
{

    BATCH_VIEW("batch_view"),
    SPEED_VIEW("speed_view"),
    SPEED_NAME_TEMPORARY_VIEW("speed_name_temporary_view"),
    SPEED_EMAIL_TEMPORARY_VIEW("speed_email_temporary_view"),
    MERGE_VIEW("merge_view");

    private final String name;

    View(String str)
    {
        this.name = str;
    }

    public String toString()
    {
        return this.name;
    }

}
