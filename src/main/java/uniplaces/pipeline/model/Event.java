package uniplaces.pipeline.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


/**
 * Created by tmateus.
 */
public class Event implements Serializable
{
    private String timestamp;
    private String action;

    @SerializedName("guest_id")
    private String guestId;
    private String name;
    private String email;

    public String getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(String timestamp)
    {
        this.timestamp = timestamp;
    }

    public String getAction()
    {
        return action;
    }

    public void setAction(String action)
    {
        this.action = action;
    }

    public String getGuestId()
    {
        return guestId;
    }

    public void setGuestId(String guestId)
    {
        this.guestId = guestId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }
}
