package uniplaces.pipeline.model;

/**
 * Created by tmateus.
 */
public enum EventType
{

    GUEST_REGISTERED("guest_registered"),
    GUEST_NAME_UPDATED("guest_name_updated");

    private final String name;

    EventType(String str)
    {
        this.name = str;
    }

    public String toString()
    {
        return this.name;
    }

}
