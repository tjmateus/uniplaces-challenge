package uniplaces.pipeline.model;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by tmateus.
 */
public class Location implements Serializable
{
    private Entity entity;
    private Fact fact;

    public enum Entity
    {
        ENTITY_NAME("name"),
        ENTITY_EMAIL("email");

        private final String name;

        Entity(String str)
        {
            this.name = str;
        }

        public String toString()
        {
            return this.name;
        }
    }

    public enum Fact
    {
        FACT_EVENT("event");

        private final String name;

        Fact(String str)
        {
            this.name = str;
        }

        public String toString()
        {
            return this.name;
        }
    }

    public Location(Entity entity, Fact fact)
    {
        this.entity = entity;
        this.fact = fact;
    }

    public String getFilePath()
    {
        return new SimpleDateFormat("yyyy/MM/dd").format(new Date()) + File.separator + this.getEntity().toString() + File.separator + this.getFact().toString() + File.separator + this.getFileName();
    }

    public String getFileName()
    {
        return this.getFact().toString() + "_" + this.getEntity().toString() + ".json";
    }

    public Entity getEntity()
    {
        return entity;
    }

    public void setEntity(Entity entity)
    {
        this.entity = entity;
    }

    public Fact getFact()
    {
        return fact;
    }

    public void setFact(Fact fact)
    {
        this.fact = fact;
    }

}
