package uniplaces.pipeline.configuration.exceptions;

import uniplaces.pipeline.exceptions.PipelineException;


/**
 * Created by tmateus.
 */
public class ConfigurationException extends PipelineException
{

    private static final long serialVersionUID = 6766418125996301214L;

    public ConfigurationException()
    {
        super();
    }

    public ConfigurationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ConfigurationException(String message)
    {
        super(message);
    }

    public ConfigurationException(Throwable cause)
    {
        super(cause);
    }

}
