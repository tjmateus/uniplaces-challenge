package uniplaces.pipeline.configuration.model;

import uniplaces.pipeline.configuration.exceptions.ConfigurationException;

import java.util.Map;
import java.util.Map.Entry;


/**
 * Created by tmateus.
 */
public class Configuration
{

    private Map<String, String> properties;


    public Configuration(Map<String, String> properties)
    {
        this.properties = properties;
    }


    public String getProperty(String property, String defaultValue) throws ConfigurationException
    {
        String value = this.properties.get(property);

        if(value != null)
        {
            return value;
        }
        else
        {
            return defaultValue;
        }
    }


    public String getProperty(String property) throws ConfigurationException
    {
        return this.properties.get(property);
    }


    public Map<String, String> getProperties()
    {
        return this.properties;
    }


    @Override
    public String toString()
    {
        StringBuilder result = new StringBuilder();
        for (Entry<String, String> entry : this.properties.entrySet())
        {
            result.append(entry.getKey()).append(" -> ").append(entry.getValue());
        }
        return result.toString();
    }
    
}
