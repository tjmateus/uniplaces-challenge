package uniplaces.pipeline.configuration;

import org.slf4j.LoggerFactory;
import uniplaces.pipeline.configuration.exceptions.ConfigurationException;
import uniplaces.pipeline.configuration.model.Configuration;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;


/**
 * Created by tmateus.
 */
public class ConfigurationFactory
{

    private static final String DEFAULT_USER_HOME_CONFIG_DIRECTORY_PATH = System.getProperty("user.home") + File.separator + "uniplaces" + File.separator + "config" + File.separator;
    private static final String DEFAULT_LOCAL_CONFIG_DIRECTORY_PATH = "/config" + File.separator;
    
    
    public static Configuration build(String configurationFileName) throws ConfigurationException
    {
        return new Configuration(initProperties(configurationFileName));
    }


    private static Map<String, String> initProperties(String configurationFileName) throws ConfigurationException
    {
        Map<String, String> propertiesMap = new HashMap<>();

        Properties properties = new Properties();

        InputStream inputStream;

        //Try to read configurations from an external file
        try
        {
            File userHomeConfigFile = new File(DEFAULT_USER_HOME_CONFIG_DIRECTORY_PATH + configurationFileName);
            inputStream = new FileInputStream(userHomeConfigFile);

            LoggerFactory.getLogger(ConfigurationFactory.class).info("Reading configuration from [" + DEFAULT_USER_HOME_CONFIG_DIRECTORY_PATH + configurationFileName + "].");
        }
        //Case there is no such file, load from resources.
        catch (FileNotFoundException e)
        {
            inputStream = ConfigurationFactory.class.getResourceAsStream(DEFAULT_LOCAL_CONFIG_DIRECTORY_PATH + configurationFileName);

            LoggerFactory.getLogger(ConfigurationFactory.class).info("Reading configuration from [" + DEFAULT_LOCAL_CONFIG_DIRECTORY_PATH + configurationFileName + "].");
        }

        if (inputStream != null)
        {
            try
            {
                properties.load(inputStream);
            }
            catch (IOException e)
            {
                throw new ConfigurationException("Could not load properties from file '" + configurationFileName + "'.");
            }
        }
        else
        {
            throw new ConfigurationException("Property file '" + configurationFileName + "' not found in the classpath.");
        }

        for (Entry entry : properties.entrySet())
        {
            propertiesMap.put(entry.getKey().toString(), entry.getValue().toString());
        }

        return propertiesMap;
    }

}
